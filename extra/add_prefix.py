# Adicionar prefixo

import sys
import os

def add_prefix(fileList, prefix):
	fileList_out = [prefix + fn for fn in fileList]
	return fileList_out

def files_rename(fnPairs):
    for pair in fnPairs:
        if len(pair) != 2:
            continue
        os.rename(pair[0], pair[1])

################################################################################

if len(sys.argv) == 1:
    print("Not enough arguments.", file=sys.stderr)
    exit(0)

dirName = sys.argv[1]
prefix = sys.argv[2]

# Fix dirname
if dirName[-1] == '\\':
    dirName = dirName[:-1]

fileListRaw = os.listdir(dirName)
fileListRaw.sort()

fileList = [fn for fn in fileListRaw if os.path.isfile(dirName + "\\" + fn)]
filesOld = add_prefix(fileList, dirName + "\\")
filesNew = add_prefix(add_prefix(fileList, prefix), dirName + "\\")

fnPairs = []
for i in range(len(filesOld)):
    fnPairs.append((filesOld[i], filesNew[i]))

files_rename(fnPairs)