#PJF csv input-output
#Based on the python csv library

def csv_read_data(csv_filename, encoding="utf_8", delimiter=","):
    if not os.path.isfile(csv_filename):
        print(csv_filename, "not found")
        return None
    data = []
    csv_fp = open(csv_filename, "r", encoding=encoding)
    csv_data = csv.reader(csv_fp, delimiter=delimiter)
    
    return [row for row in csv_data]
