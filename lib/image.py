import numpy as np

def add_border(img_in, border):
	H, W = img_in.shape
	
	img = np.zeros((H+border*2, W+border*2), uint8)

	# Brute force
	# FIXME Devia apenas considar os pontos de fronteira

	for i in range(H):
		u = i + border
		for j in range(W):
			v = j + border
			if img_in[i,j] == 0:
				continue
			
			# FIXME Verificar os erros 1-off
			for u in range(-border, border):
				dv = int((border**2.0 - u**2.0)**0.5)
				i1 = i + u + border
				for v in range(-dv, dv):
					j1 = j + v + border
					img[i1,j1] = 1
	
	# FIXME Falta fazer o crop à medida?
	
	return img
