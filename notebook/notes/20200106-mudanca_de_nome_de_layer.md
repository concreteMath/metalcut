2020-01-06

# Mudança de nome de layer
De modo a mudar o nome de um layer é necessário ir actualizar vários aspectos do modelo, de modo a manter a coerência entre eles.
Para mudar o nome de um layer, vai-se à caixa dos layers, selecciona-se o layer e faz-se um click esquerdo. Muda-se o nome.
No entanto é necessário actualizar a página de plot correspondente. Para isso, vai-se à caixa do "Plotview Manager", clicka-se no primeiro ícone ("Plotview Pages") e escolhe-se "Restore default page name". Isto vai renomear as páginas conforme o nome do layer correspondente.
Também é preciso ir ao Assembly Managera actualizar as propriedades das peças. Clicka-se com o botão direito no ícone "Part list", escolhe-se "Select Elements" e em seguida clia-se com o botão direito numa das peças abaixo e escolhe-se "Set value by layer features". Dentro da nova caixa tem de estar seleccionada a opção "Layer Name" e selecciona-se a opção "Field 1: Description" -> [OK].
Clica-se outra vez com o botão direito numa peça qualquer e selecciona-se "Apply splitter". Na nova caixa o separador usado deve ser o "Other" com o símbolo "$". Selecciona-se o item "Splitters/PJF" da árvore e clica-se no botão "Apply splitter" (marca de visto).
