2020-02-19

# Porta matrizes normalizados - alívios
Quanto temos uma caixa em que as paredes forma esquinas (de raio 0), há que aliviá-las por causa das limitações da maquinação (é impossível fazer raios 0 interiores).
![porta matrizes normalizados alivios](img/20200219-porta_matrizes_normalizados.svg)

O raio do alívio deverá ser pelo menos 2mm (talvez 3mm) e deve ser introduzido de maneira a apanhar a esquina por dentro do material, mas de tal modo que não elimine muito à face de encosto (máximo 1mm).

No caso de ser necessário desbastar metal temperado ou solda, é conveniente ter o alívio com um diâmetro Ø6mm, de modo a se puder usar uma broca de Ø6mm, a qual já é bastante robusta.
