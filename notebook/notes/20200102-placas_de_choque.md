20200102

# Placas de choque
## Material
O material usado costuma ser o mesmo para todas as ferramentas: 1.8719 (alternativa 1.0601)

## Espessura
A espessura das placas de choque costuma seguir valores standard das placas de aço, menos 0.5mm para ter margem para rectificar.
As espessuras típicas das placas de aço são:
5, 6, 8, 10, 12, 15, 20, 25, 30, 40, 50 mm
