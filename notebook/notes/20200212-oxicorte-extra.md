2020-02-12

# Oxicorte - extra
- Seguir o guia do oxicorte
EXTRA:
- Caso um furo toleranciado tenha mais de Ø30mm, deveria ser cortado, mas dado que é necessário dar -5 até -8mm de aperto, se este ficar muito pequeno, mais vale eliminá-lo.
- Caso um limitador de curso passe perto do tecto, dar-lhe um extra de material (no tecto) de 3mm. (isto deverá depender da espessura)
