2020-02-14

# Jacto de Água
No fim de enviar os ficheiros para o responsável de compras (João), imprimir cada ficheiro em formato A4 (ou num tamanho que se veja) para lhe entregar também.
Ele usa estas impressões para fazer o controlo da entrada do material.
