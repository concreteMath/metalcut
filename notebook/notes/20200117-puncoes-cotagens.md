2020-01-17

#Punções - cotagens
A cotagem de um punção é bastante simplificada. Basta ter uma vista de cima e um corte que mostre um dente que possa ter.
As cotagens a indicar sao:
- A largura total
- O comprimento total
- A altura total
- A altura do extractor
- A altura do roscado dos parafusos
- A classe dos parafusos

Se tiver dente, deve-se indicar:
- A largura da cavidade do dente
- O desnível do cimo do punção (plano de entrada dos parafusos) até ao início da cavidade do dente
![exemplo vistas punção](img/20200117-exemplo_vistas_puncao.svg)
