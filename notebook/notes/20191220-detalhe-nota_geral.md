2019-12-20

# Nota geral
Vários clientes pedem uma marcação com vários detalhes sobre a peça.
![caixa detalhes peça](img/20191220-caixa_detalhes_peca.svg)

onde $obra é o código da obra, $posicao é a posição na ferramenta, $material é o material da peça e $tratamentoTermico é o tratamento térmico que a fundição dá à peça.
