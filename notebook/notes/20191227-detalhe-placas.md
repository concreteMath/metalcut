2019-12-27

# Placas
(penso que isto seja em relação à placa da base e do tecto)

## Tolerâncias
Meter tolerâncias em:
- caixas (peças cota com cota)
- casquilhos
- cavilhas

Como as placas são quase totalmente maquinadas em CNC, então basta marcar marcar posições de operações que serão executadas à mão, como por exemplo os roscados das alcaiatas.

Devem-se introduzir dados/cotas que sejam úteis (à bancada e não só)
