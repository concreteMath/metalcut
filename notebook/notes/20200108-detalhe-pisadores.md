2020-01-08

# Detalhe - pisadores - cotagem
Além dos elementos de cotagem habituais (posição de centros de parafusos, cavilhas, pilotos e limites de caixas), devem ser marcados os ângulos do chanfro na abertura dos pilotos e a zona de sucata a cortar no fio com furo de enfiamento para sucatas interiores.
Os pilotos devem ter um diâmetro com tolerância H7.
Adicionar a nota:
![nota pisador](img/20200108-nota_pisador.svg)

texto:
`height: 2.5`
`lines gap ratio: 0.25`

As zonas de corte de fio devem ser assinaladas com uma malha quadrada a 45º com 5mm de largura de malha. O furo de enfiamento deve ter diâmetro 6 e o seu centro deve distar 6mm da linha de corte. Idealmente, o furo de enfiamento deverá estar perto de uma linha de corte direito, para facilitar a rectificação.
