2020-01-14

# Confirmação da unicidade do tipo de peça por layer
Para verificar que cada lauer um tipo de peça única (peças simétricas são tipos diferentes), pode-se fazer uma inspecção visual no VISI.
Começa-se por desligar todos os layers
`Painal dos layers > tab "Layer" > botão "set-reset layers"`
Vai-se para o ecrã de vista 3D.
Roda-se o modelo 3D até ficar num ângulo que fique quase de cima, de modo a que se perceba a profundidade nas peças.
Kigar o enquadramento automático do modelo com o botão "Zoom mode" (no painel dos layers).
Ainda no painel dos layers, clicka-se no primeiro layer que contenha peças (ex: `A1101$SubBase`)
Carrega-se no botão para baixo do teclado, começando assim a inspecção visual.
Se algum layer apresnetar mistura de tipos de peça, será necessário criar layers para cada tipo único.
