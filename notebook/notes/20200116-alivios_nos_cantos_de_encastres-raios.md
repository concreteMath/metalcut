2020-01-16

# Alívios nos cantos de encastres (raios)
Peças que entrem dentro de outras com folgas muito apertadas poderão ter raiso que dificultam o deslizamento do encaixe.
Para contornar esse problema, pode-se aumentar o raio dos raios da peça interior ou reduzir os raios da peça exterior.
A diferença de raios pode ser na orde de 0.2 a 0.5 mm.
Outra razão para alterar os raios deve-se à compensação dada para levara as peças ao tratamento térmico.
Normalmente dá-se uma espessura extra de 0.1mm às peças para que após o tratamento térmico se possa rectificar as peças. O problema é que os raios não são rectificados, ficando com 0.1 mm extra.
![raio extra para rectificar](img/20200116-raio_extra_para_rectificar.svg)

Assim sendo, pode-se aumentar o raio do original +0.1mm (acho eu, ainda não fiz cálculos)(acho que já fiz o cálculo, procurar), compensando a compensação.
