2019-12-26

# Detalhe de cepo de reacção
De modo a evitar colisões, a régua deve ser encostada ao cepo de modo a que a tolerância entre no material do cepo. Portanto d = x 0/-0.02
![cepo de reaccao e regua](img/20191216-cepo_de_reaccao_e_regua.svg)
