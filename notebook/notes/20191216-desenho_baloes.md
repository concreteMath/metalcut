#Desenho
#Peça: geral

## Marcação da parte de baixo/cima com balões
(20191216) (desenho de conjunto)
- Basta marcar um dos elementos da mesma posição
- Os elementos normalizados (normalmente pos >= 500) devem ter um balão dividido ![balao](desenho-conjunto/balao_normalizado.svg), diferenciando-os assim dos restantes componentes.
