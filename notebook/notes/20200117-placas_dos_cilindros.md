2020-01-17

# Placas dos cilindros
Estas placas colocam-se entre os cilindros e o guia móvel, de modo a atenuar o desgaste que os cilindros fazem no guia móvel.
A sua altura não precisa de tolerância, dado que o veio do cilindro se adapta a qualquer altura que a placa tenha.
