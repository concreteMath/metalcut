2020-03-11

# Tipos de letra para exportação DWG
Quando se exportam folhas com texto, convém formatar o texto o alinhamento "Left alignment" e "Top alignment", font "Courier New" e com uma relação Height/Width = 1.5
Todas estas formatações irão manter a consistência entre o VISI e a exportação em DWG.

Extra: Os parâmetros "Char gap" e "Lines gap ratio".
(ACABAR?)
