2020-01-14

# Placas de choque - impressao e layout
É recomendável ter um página especial só para os desenhos a enviar para o jacto de água.
Ao não meter vários desenhos ou páginas na mesma "plotview page", é possível usar a opção "Extension" do "Plot output scale" do painel "Print", facilitando a impressão em massa de páginas.
