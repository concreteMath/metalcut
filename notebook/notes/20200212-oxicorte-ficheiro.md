2020-02-12

# Oxicorte - ficheiro
Cada placa terá um ficheiro próprio.
O ficheiro deve ter um título dentro dele próprio:
```
$obra - $ref $desc
Oxicorte - $quant peça(s) de cada
Material: $mat
Espessura: $esp mm
```

`$obra` : número de obra (ex: `1901013`)
`$ref` : referência interna da peça (ex: `A1301`)
`$desc` : descrição da peça (ex: `Base`)
`$quant` : número de peças a cortar (ex: `1`)
`$mat` : material da peça (ex: `80`)
(o texto pode ter tamanho 25)

O ficheiro deve ter o nome:
`$obra__$ref_$desc_$matName_${esp}mm__$data`
em que
`$matName` : nome descritivo do material (ex: `St52`)
`$data` : data da criação do ficheiro (ex: `20191230`)

Os ficheiros devem ser guardados na directoria
`\\pjfserver\e\2.MEGADIES\3.DESENVOLVIMENTO\2.DOSSIER DE PRODUÇÃO\OBRAS $ANO\$OBRA\Subcontratos\Oxicorte\$data`
em que
`$ANO` : Ano da obra (ex: `2019`) corresponde aos primeiro 2 dígitos da `$OBRA` com o prefixo do século
