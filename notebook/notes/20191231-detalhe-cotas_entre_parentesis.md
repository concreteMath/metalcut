2019-12-31

# Cotas entre parêntesis
(ex: (x.xxx))

## Pisadores
A cota indica o valor teórico, faltando a compensação para pisadores (ex: 0.5mm)

## Batentes do guia móvel
A cota da altura dos batentes poderá estar entre parentesis. Isso indica que a altura (não a tolerância) poderá precisar de ser acertada conforme a ferramenta.
