2020-01-14

# Placas de Choque (tolerâncias)
O rigor que é necessário garantir na rectificação da espessura de uma placa de choque depende da sua função.
Se a placa de choque estiver entre o tecto e um punção de forma, há que garantir uma espessura com bastante rigor, dando-se uma tolerância x(+/-0.02).
Se a placa de choque estiver entre o tecto e um punção de corte, o mais importante é a garantia de paralelismo entre a face que encosta no tecto e a face que encosta no punção. Se houver falta de paralelistmo, o erro de distância entre as duas faces é amplificado pela altura do punção, podendo lever a um desvio tal que leva o punção roçar na matriz.
![placa de choque do puncao](img/20200114-placa_de_choque_do_puncoa.svg)

Sendo assim, dá-se uma tolerância de x(+/-0.01) na espessura da placa de choque.
