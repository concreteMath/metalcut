obra: 
## Ferramenta
A ferramenta é o objecto objectivo de cada obra que se vai vender ao cliente.
É formada por um conjunto de componentes.
É organizada em 4 grandes zonas.

## Sinex
O Sinex é o sistema usado na PJF para organizar o processo de construção de um componente.
Na base de dados do sinex constam as características de cada componente, bem como a fase do seu progresso.
Com este sistema, é estimado o tempo de maquinação, orçamentação.