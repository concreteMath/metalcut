2020-01-31

# Construção do ficheiro de corte laser de banda
A banda cortada a laser serve para testar as matrizes de estampagem.
Esta é construída pela repetição do passo imediatamente antes da primeira estampagem, enquanto a banda está plana.

## Construção
Começa-se por extrair o contorno do passo de interesse, tal que o seu comprimento seja igual ao comprimento do passo.
Copiam-se as curvas extraídas para longe da ferramenta.
Cria-se um novo layer para estas curvas, com o nome `#Primeira proposta de banda corte laser` ou algo semelhante.
![corte laser curvas passo](img/20200131-corte_laser_curvas_passo.svg)

Depois fazem-se cópias das curvas do passo, distanciando entre si o tamanho do passo, até que esta banda tenha uma dimensão menor que uma dada medida máxima (ex: 2m).
Para garantir a qualidade desta construção da banda, dever-se-à criar uma polyline do perímetro da banda, de modo a garantir que não existem segmentos interrompidos, o que iria afectar o corte do laser.
![corte laser banda](img/20200131-corte_laser_banda.svg)

Se forem necessárias mais que uma banda, fazem-se cópias da banda, ficando paralelas entre si, guardando 5-10mm de separação entre elas.
Por cima das bandas escreve-se algo como:
`1901015-primeira proposta de banda para corte laser - 2020/01/31`

## Envio do desenho
Exportam-se as bandas e o texto para um DXF, o qual é colocado na pasta
`$prod\$projecto\Subcontratos\Laser\$data`
em que
`$prod` : pasta de produção
`$projecto` : código da obra (ex: 1901015)
`$data` : dada da criação da banda (ex: 2020-01-31)

Finalmente, envia-se um mail ao responsável das compras (João), a dizer:
`O ficheiro para o corte laser da primeira proposta da banda para a ferramenta $projecto está na pasta $pasta`
