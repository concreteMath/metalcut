2020-01-14

# Batentes inferiores e estabilizadores - altura
Em princípio, a altura destes elementos deve ser bastante rigorosa, o que obriga uma tolerância x(+/-0.02).
No entanto, estes elementos poderão sofrer ajustes na montagem, ou seja, a cota `x` poderá ser outra. Então, indica-se a cotagem como `<x(+/-0.02)>`.
