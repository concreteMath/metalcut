2020-01-02

# Detalhe do Bloco de Punções
Começa-se por criar vistas de topo de cada punção da ferramenta.
Depois, tenta-se obter uma configuração que minimize o volume do bloco que os contém.
![configuração do bloco de punções](img/20200102-configuracao_bloco_de_puncoes.svg)

Os punções devem distar no mínimo 8mm entre eles.
É necessário introduzir 1 ou mais furos de enfiamento de modo a poder cortar no fio.
O bloco precisa de mais 8mm em 2 lados opostos e 16mm nos outros dois lados.
Os lados mais espessos levam um degrau de 8mm de altura e largura para fixar na máquina do fio.
![corte bloco de punções](img/20200102-corte_bloco_de_puncoes.svg)
