2019-12-30

# Furações para parafusos
Para um parafuso de classe Mx (ex: M8), a profundidade da rosca deve ser `2*x` a `2.5*x` mm.
Para furações laterais usadas para levantar peças (peças pequenas), pode-se usar M12 com uma profundidade entre 15 e 20mm. Para peças mais pesadas usar M16 com profundidade de 24mm.

## Furações para segurar as alcaitas
As furações devem ser posicionadas nas faces laterais das peças e nos cantos da face superior, de tal modo que não intersectem outras furações.
![furacoes para alcaiatas](img/20191230-furacoes_para_alcaiatas.svg)

Se possível ou necessário, as furações verticais podem ser fora-a-fora.

