2020-01-16

# Porta Punções - Um porta para múltiplos punções
Quando se monta um punção numa ferramenta, a posição é ajustada pela ajuste de posição do seu porta-punções.
Porém, se vários punções partilharem o mesmo porta-punções, estes não poderão ser ajustados independentemente. Portanto, é necessário garantir que o posicionamente entre eles seja rigoroso.
Para esse efeito, impõe-se uma referência no porta-punções (como um dos cantos) denotando-se como os símbolos ![alinhamento A](img/20200116-alinhamento_A.svg) ![alinhamento B](img/20200116-alinhamento_B.svg)

![alinhamento no canto do porta](img/20200116-alinhamento_canto_porta.svg)

Para cada caixa no porta-punções é necessário indicar que um dos seus cantos tem que estar posicionado om rigor em relação à referência.
Pode-se indicar essa relação como os símbolos
![referencia A](img/20200116-referencia_A.svg)
![referencia B](img/20200116-referencia_B.svg)
![caixa com referencias](img/2020016-caixa_com_referencias.svg)

Caso as caixas as caixas estejam alinhadas pelo mesmo x e y, pode-se colocar o símbolo na linha da cotagem
![caixas alinhadas com referencia](img/20200116-caixas_alinhadas_com_referencia.svg)
