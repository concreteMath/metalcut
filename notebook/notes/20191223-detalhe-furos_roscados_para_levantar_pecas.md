2019-12-23

# Furos roscados para levantar peças
Para peças com mais de 5Kg ou que sejam encastradas (com mais de 200g), devem-se roscar furos de passagem de parafusos de modo a poder manobrar as peças ou sacá-las do encastre.
