20200114

# Plotview - pré-visualização de mudanças
As folhas da tab "General" do "Plotview Manager" por vezes ficam a vermelho, o que significa que o modelo 3D de referência a algum dos "Views" da folha sofreu alterações.
Se se usar o comando "Update selected drawings" (do menu "Plotview"), as actualizações serão irreversíveis.
Uma maneira de actualizar os desenhos com a possibilidade de fazer "undo" é pela activação da opção "Advanced lines checking".
Para isso, vai-se ao menu "Plotview" > "View properties" e seleccionam-se as "views" a actualizar (ou todas). Na caixa "Edit View", selecciona-se (ou remove-se) a opção "Advanced lines checking" e faz-se OK.
Para ver quais foram as mudanças nos desenhos, pode-se clickar em "UNDO" e depois em "REDO" várias vezes. As partes que "piscarem", foram as partes alteradas.
