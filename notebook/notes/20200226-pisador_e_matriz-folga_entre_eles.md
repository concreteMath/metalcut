2020-02-26

# Pisador e Matriz - folga entre eles
Vamos supor que temos um pisador que se eleva, usando colunas para guiar e cilindros para levantar.
Neste caso, temos a matriz a passar no interior do pisador.
![pisador e matriz](img/20200206-pisador_e_matriz.svg)

A ferramenta ao fechar, pode não pressionar uniformemente sobre os cilindros. Tendo apenas 2 cilindros para guiar, se os cilindros comprimirem a velocidades diferentes, podem torcer ou rodar o pisador. Este efeito irá cansar os componentes envolvidos, especialmente as colunas e os casquilhos (acho eu).
Uma forma de mitigar este problema passa pela introdução de uma folga mais ou menos apertadas entre o pisador e a matriz. Esta não pode ser muito apertada, senão estarão quase sempre em contacto, nem pode ser muito alargada, senão o efeito correctivo é perdido.
Visto de outro ponto de vista, esta estratégia faz com que a matriz seja uma reacção em caso de haver desalinhamento no guiamento.
