2020-01-02

# Calço para compensação de altura de punção
Quanto temos um porta-matriz normalizado, por vezes não é possível chegar a uma altura desejada (de porta + matriz).
Então, vamos precisar de dar a altura em falta, a qual pode ser dada pela placa de choque.
No entanto, para alturas de compensação muito altas, como as cavilhas que ligam o porta matriz ao tecto passam com folga através da placa de choque, pode ser que a matriz deslize graças à tal folga.
Assim sendo, uma outra solução passa por adicionar uma nova peça (do mesmo material da placa de choque) à qual se vai agarrar o porta-metriz e a qual se vai agarrar ao tecto.
Portanto, pode-se pensar nesta peça como uma extensão/altura extra do tecto.
Pode-se mandar cortar tal peça no jacto de água, mas apenas o seu contorno.
