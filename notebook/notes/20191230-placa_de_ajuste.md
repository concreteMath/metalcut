2019-12-30

# Placa de ajuste
As placas de ajuste devem ter a sua altura toleranciada de acordo com a função que vão cumprir.
![corte placa de ajuste](img/20191230-corte_placa_de_ajuste.svg)

A placa de elevação 1 em conjunto com a base e o elevador de banda têm de garantir uma altura rigorosa para o assentamento da banda. É também necessário garantir o paralelismo das faces inferior e superior da placa de elevação, eliminando inclinações no assentamento da banda.
O material encomendado para estas placas obedece a uma tabela de espessuras standard.Então, de moda a ter algum espaço de ajuste, definem-se uma espessura 0.5mm abaixo de uma espessura standard (ex: 6.0mm), podendo assim rectificar ambas as faces ~0.25mm.
Em termos de tolerância, dá-se +/-0.02mm, minimizando o erro da altura do conjunto base + placa + elevador.

Em relação à placa de ajuste 2, essas preocupações não existem, podendo até ter uns 0.5-1mm de erro.

## Placas de choque
No caso das placas de choque, que estão entre os porta punções e o tecto, o mais importante é garantir o paralelismo entre faces. A espessura é uma preocupação secundária, dado que o punção pode entrar mais ou menos umas décimas. No entanto se o paralelismo estiver mal feito, faz com que o punção fique inclinado e entre umas décimas ao lado na matriz.
Há que chamar a atenção da bancada quanto a estas questões, usando a indicação da tolerância.

