2020-02-19

# Porta punções normalizado
A profundidade da cavidade onde encaixa a base do punção normalizado deverá ter +0.3mm que a altura indicada nas especificações do punção.
![cavidade do puncao](img/20200219-cavidade_do_puncao.svg)

Esses +0.3mm extra podem ser dados na tolerância com x(+0.4/+0.2)mm.
Esse extra server para que algum raio que possa existir entre a base e o corpo do punção seja acomodado pelo porta, evitando que a base sobressaia além do porta.

## Raio mínimo
Em relação ao outro plano da caixa (x,y), o raio mínimo que deve ser usado deverá ser o raio que possa ser executado nas CNC, acima de 2mm.
![cavidade do puncao raio minimo](img/20200219-cavidade_do_puncao_raio_minimo.svg)
