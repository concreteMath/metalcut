2020-03-01

# Punções e matrizes - dimensionamento
O dimensionamento de um punção e de uma matriz para obter determinado diâmetro (ou dimensão) segue um critério dado pelo cliente.
No caso da Gestamp, se tivermos um diâmetro de furo `Øfuro` com uma tolerância inferior `ti` e uma tolerância superior `ts`, então o dimensionamento do punção é obtido por:
(caderno de encargos da gestamp, pág.65)
`Øpuncao = Øinf + 3/4*dt` (valor arredondado à décima)
em que
`Øinf = Øfuro + ti`
`dt = ts - ti`

No caso das matrizes, o dimensionamento é:
(caderno de encargos da gestamp, pág.70)
`Ømatriz = Øpuncao + 2*r*e`
em que
`r` é o factor de folga
`e` é a espessura da chapa

O valor de `r` depende da dureza da chapa (N/mm^2). Para chapas standard na PJF, usa-se `r=0.1` (dureza <= 250 N/mm^2).
