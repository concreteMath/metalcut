2019-12-16

# Configuração das dimensões e balões
Antes de começar a detalhar, há que configurar o VISI para que o tamanho das cotações fique correcto.
Vai-se ao menu "Annotation">"Local dimensions parameters" e aplica-sem as seguintes configurações:
- (General Values) Height: 2.5
- (Balloons) Circle Radius: 8
