2020-01-14

# Cotagens - raios e diâmetros
Só se usa uma cotagem com um diâmetro para círculos completos. Se forem segmentos de arco, usa-se uma cotagem com raio.
