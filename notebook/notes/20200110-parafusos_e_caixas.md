2020-01-10

# Parafusos e caixas
![parafusos e caixas altura](img/20200110-parafusos_e_caixas_altura.svg)
A espessura mínima para `h` deve ser 2/3mm.
Idealmente, todas as caixas de uma peça deverão ter o mesmo valor de `h`, minimizando o número de tipo de  parafusos a usar.
![parafusos e caixas espessura](img/20200110-parafusos_e_caixas_espessura.svg)
A espessura de parede (`d`) deverá também ter no mínimo 2/3mm.
![parafuso e caixas inclinado](img/20200110-parafusos_e_caixas_inclinado.svg)
Caso a cabeça de um parafuso fique de fora, há que garantir que outras peças que estejam na proximidade não colidam com o parafuso.
