2020-01-07

# Detalhe Réguas Guia (tolerância)
Uma régua-guia costuma ter um rasgo onde a a chapa enconsta. As dimensões desse rasgo devem ter uma tolerância x(+0.1/0)
![reguas guia tolerancias](img/20200107-reguas_guia_tolerancias)
