20200212

# Oxicorte - detalhe

## Espessura da placa a encomendar
A placa a encomendar terá de ser maquina para ser ser desempenada/rectificada. Portanto, terá que se dar uma espessura extra à placa. Além disso, existe uma tabela de espessuras normalizadas.
Seja `$esp_proj`, a espessura da placa no projecto e `$esp_extra`, a espessura extra, então a espessura a pedir será a espessura tabelada mínima, tal que seja maior que `$esp_proj + $esp_extra`.
`$esp_extra` é 4mm para placas com dimensão máxima maior que 1000mm.

## Cotagens
Cotar as dimensõs máximas (tipo bounding-box) em x e y.
![oxicorte cotagens maximas](img/20200212-oxicorte_cotagens_maximas.svg)

## Compensação nos encaixes da sub-base
Os encaixes laterais da sub-base requerem algum rigor de modo a ficarem bem enquadrados com a prensa. Como o oxicorte não tem muito rigor no corte, então terá de deixar algum material para se poder maquinar as superfícies que requerem rigor.
(Creio que existam alguns valores tabelados da imprecisão do oxicorte, o que seria interessante para calcular a margem de segurança.)

No caso de um encaixe redondo, elimina-se o encaixe
![oxicorte encaixes redondos](img/20200212-oxicorte_encaixes_redondos.svg)

No caso de um encaixe rectangular, dá-se um offset de 6mm à face de encosto.
![oxicorte encaixes rectangulares](img/20200212-oxicorte_encaixes_rectangulares.svg)
