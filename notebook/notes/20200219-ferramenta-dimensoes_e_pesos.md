2020-02-19

# Ferramenta - dimensões e pesos

## Passo
O passo é medido entre os pilotos ou as furações dos pilotos. Também pode ser medido entre pontos em que o mesmo o mesmo ponto da banda toque, dividindo essa distância pelo número de postos entre os dois pontos.

## Largura da Banda
Pode ser medida entre as zonas de encosto das réguas.
![largura entre réguas](img/20200219-largura_entre_reguas.svg)

## Altura da Alimentação
Esta corresponde ao ponto intermédio onde a banda assenta durante um ciclo.
Esta pode ser decomposta como a soma de duas medidas: o desnível desde a parte inferior da sub-base até à parte superior das matrizes de corte e metade da distância de curso da elevação.
A distância de curso da elevação pode ser obtida a partir da referência de um cilindro que empurra a elevação.
Exemplo:
`N3101$GasSpring$Azolgas$CW 170 100 V1 (80)`
Este cilidro tem um curso de elevação = 80mm
![altura da alimentacao](img/20200219-altura_da_alimentacao.svg)

=> `Altura Alimentação = H + 1/2*h`

## Prensa
Obtida a partir das especificações do cliente.

## Dimensões máximas
Para obter as dimensões máximas da ferramenta:
- vai-se ao VISI
- ligam-se os grupos de layers: _Lower Die_, _Elevation_, _Mobile_ e _Upper Die_
- seleccionam-se tudo
- calcula-se o bounding-box

## Peso da Ferramenta (Parte inferior)
Activar os grupos de layers _Lower Die_ e _Elevation_ e calcular o peso (menu ? > Weight).
Usar "Ordinary Steel" (7.85 g/cm^3).

## Peso da Ferramenta (Parte Superior)
Fazer o mesmo que na parte inferior, mas para os grupos de layers _Mobile e Upper Die_.
