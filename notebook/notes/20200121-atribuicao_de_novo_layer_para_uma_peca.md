2020-01-21

# Atribuição de novo layer pra uma peça
Quando se muda o nome de um layer ou quando se atribui uma layer novo para uma peça, é necessário actualizar a informação da mesmo no _assembly manager_.
Vai-se ao painel do _Assembly Manager_, e à tab do _Parts list view_, seleccionam-se as peças mudadas, clica-se com o botão direito e escolhe-se _Apply splitter_.
Na caixa _Splitter_, escolhe-se o item _PJF_ da árvore de Splitters e carrega-se na opção _Apply Splitter_. A opção _Other_ (`$`) deve estar seleccionada na zona dos _Separators_.
