2019-12-17

# Detalhe Cavilhas
- As cavilhas são identificadas por círculos azuis-escuros, mas nem todos os círculos azuis escuros são cavilhas. Estas também podem ser identificadas no modelo 3D.
- A identificação definitiva é dada pela furação (de cavilha) feita na peça que assenta nessa posição.
