2020-01-10

# Parafusos, caixas e tolerâncias apertadas
Quando uma peça encastrada noutra tem uma folga muito apertada à caixa onde encaixa, dá jeito roscar o "pescoço" de uma caixa de um (ou mais) parafusos.
O roscado deve ter um diâmetro interior de tal maneira que deixe passar o parafuso da caixa.
(ESTA EXPLICAÇÃO ESTÁ MUITO CONFUSA)
![roscado passante](img/20200110-roscado_passante.svg)
