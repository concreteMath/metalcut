2020-01-21

# Save de peças individuais - multi peça
Quando se gravam as peças, layer por layer, é preferível gravar todas as peças desse mesmo layer do que apenas uma só peça.

Quando de desenham peças simétricas, estas deveriam ficar em layers diferentes. No entanto, pode acontecer que as peças simétricas fiquem no mesmo layer. Se se gravar apenas um elemento desse layer e se se der a indicação de que são _n_ cópias, vão ser produzidas _n_ cópias da peça enviada.
Se se gravar todas as peças do layer, mesmo que se esqueça de fazer a separação das peças diferentes em layers diferentes, a produção será correcta.
