# Códigos

## Código de obra
Uma obra é identificada por um número, o qual está dividido em três partes:
- ano (2 dígitos)
- tipo de obra (2 dígitos)
- número de série em função do ano e tipo de obra (?) (3 dígitos)

Por exemplo, se tivermos uma obra de 2019 do tipo 1 e com número de série 13, fica 1901013.

## Códigos de peça
A ferramenta é formada por um conjunto de componentes, em cada um dos componentes tem uma identificação única. 

### Referência interna
Dentro da PJF, cada componente é identificado por uma referência interna, a qual é dividida em duas partes: uma letra e um número de 4 dígitos, por exemplo A1301.
A letra refere-se à localização vertical dentro da ferramenta, sendo o A correspondente aos componentes da base, o B aos componentes da elevação e parte móvel e o C aos componentes do tecto.
O número é um número único dado ao componente dentro de cada letra. Os primeiros dois dígitos correspondem a tipos de componente típicos numa ferramenta (ex: matrizes de corte, pisadores) e os dois dígitos finais é um número de série dado a cada componente pertencente a uma dada letra e tipo de componente.
A referência interna é o sistema usado no Sinex.

### Posição
A posição é um número de série único dado a cada (?tipo) de componente da ferramenta.
A bancada costuma usar este número para identificar os componentes.
Este é o sistema usado nas comunicações com os clientes.