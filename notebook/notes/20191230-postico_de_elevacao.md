2019-12-30

# Postiço de elevação
O postiço de elevação serva para elevar a banda e guiar o piloto.
Deve correr no porta matriz.
Como cada pessoa na bancada tem o seu método de trabalho, deve-se aplicar uma tolerância generosa, deixando bastante material. Assim sendo, o furo tem tolerância H7 e o macho fica com tolerância g6.
![postico de elevação](img/20191230-postico_de_elevacao.svg)
