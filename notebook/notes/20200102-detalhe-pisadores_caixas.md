2020-01-02

# Pisadores (caixas)
As caixas de parafusos dos pisadores deverão ter uma profundidade x+δ, em que x vem do tamanho do parafuso Mx e δ é uma profundidade extra.
Essa profundidade extra depende do tamanho do parafuso e serve para introduzir uma anilha-mola, ajudando a fixar o parafuso, prevenindo a sua queda e consequente danificação da ferramenta no momento da estampagem.
Esse δ costuma andar na ordem dos 2-4mm.
