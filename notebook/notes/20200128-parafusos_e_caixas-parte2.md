2020-01-28

# Parafusos e caixas - parte 2
Quando se fazem caixas de parafusos para peças que são presas de "pernas para o ar" (ex: Batente no tecto) é costume o cliente pedir uma segurança extra para o parafuso não cair durante a estampagem.
O sistema usado é o "Nord-Lock" que consiste numa "anilha especial" que se coloca no parafuso para que este não desenrosque fácilmente.
![caixa parafuso com nordlock](img/20200128-caixa_parafuso_com_nordlock.svg)

Assim sendo, a profundidade da caixa é
`H = h0 + h1 + h2`
em que
`h0` : altura de segurança
`h1` : altura da cabeça do parafuso
`h2` : altura da anilha "nord-lock", de acordo com o tamanho do parafuso
