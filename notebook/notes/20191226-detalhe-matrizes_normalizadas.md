2019-12-16

# Matrizes normalizadas
A caixa de encastre deverá ter uma tolerância F7. Caso a matriz tenha uma face
![caixa de matriz normalizada com face](img/20191216-caixa_matrix_norm_face.svg)

a distância entre a face e o centro deverá ter uma tolerância 0/+0.02

Quanto ao posicionamento vertical, é necessário garantir que o plano superior do porta-matriz e da matriz tenham uma posição rigorosa (ex: 35 +/-0.02).

Como a matriz normalizada tem a altura com uma medida rigorosa, então basta garantir que a altura de material que a suporta por baixo também tenha a tolerância +/-0.02.
![corte matriz normalizada](img/20191226-corte_matriz_normalizada)

Caso a cota da espessura do porta-matriz tenha uma tolerância rigorosa, então pode-se cotar a altura da face superior até ao degrau onde assenta a matriz.
![corte matriz normalizada 2](img/20191226-corte_matriz_normalizada_2.svg)


## CAM em matrizes normalizadas
Se uma matriz normalizada tiver operações de CAM, as quais podem resultar de um CAM conjunto com o seu porta-matriz, então pode-se indicar nos desenhos do porta-matriz, a colocação da matriz em estilo tracejado. Também deve-se deixar uma indicação do código ou posição da matriz.
![cam conjunto matriz e porta](img/20191226-cam_conjunto_matriz_e_porta.svg)
