2020-01-02

# Jacto de água - placas de choque
Os desenhos a enviar para o jacto de água devem seguir os seguintes critérios:
- Contornos exteriores e furos no nominal
- Furos toleranciados ou com caixa cónica ou cilíndrica não são para cortar, devendo ser eliminidados do desenho
- Para cada peça, adicionar um "label" com o código respectivo (texto com 4mm de altura)
![jacto de água label](img/20200102-label_jacto_de_agua.svg)
- Adicionar à página um bloco de texto com informações sobre o corte (texto com 7.5/8mm de altura e "lines gap ratio" com 0.6)
```
$obra - $peça1/$peça2/.../$peçaN   Placas de Choque
Jacto de Água - 1 peça cada
Material: $ref_material   $h mm de espessura
```
Exemplo:
```
$obra = 1901015
$peça1 = C5101
$ref_material = 1.8719
$h = 10
```
- Garantir uma distância mínima de 10mm entre peças, na montagem com várias peças
