2020-01-20

# Pontos de RPS
Os pontos de RPS são pontos definidos na peça para criar um referencial 3D, a partir do qual todos os outros pontos são medidos. Se estes pontos não estiverem no sítio correcto, o referencial estará mal definido e qualquer outro ponto será medido com erro.
Para minimizar este problema, são criados postos na ferramenta para corrigir a posição dos pontos RPS. Nesses postos, podem existir postiços que podem ser afinados até se encontrar uma posição para os mesmos, que minimize o erro da posição dos pontos de RPS.
