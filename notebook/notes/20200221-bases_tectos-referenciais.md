2020-02-21

# Bases/Tectos - Referenciais
Em bases e tectos é necessário definir um referencial em relação ao qual todas as operações/maquinações são efectuadas.
Pode-se definir um referencial por duas faces ortogonais da peça, geralmente as faces exteriores. No desenho assinalam-se como:
![referencias em faces](img/20200221-referenciais_em_faces.svg)
A face indicada com A é o referencial das coordenadas `x` e a face indicada com B é o referencial das coordenadas `y`.

Quando se quer indicar um ponto com rigor (ex: centro de uma coluna) mete-se uma indicação:
![referencias de pontos](img/20200221-referencias_de_pontos.svg)

Quando se quer indicar uma coordenada com rigor (ex: posição de um dos lados de uma caixa), usa-se:
![referencias de coordenadas](img/20200221-referencias_de_coordenadas.svg)
Os números nas etiquetas indicam tolerâncias do tipo `x(+dx/-dx)` (ex: `dx=0.02`, `dx=0.05`)

Como alternativa ao referencial `A|B` definido por duas paredes, pode-se maquianar um furo no centro (ou outro ponto conveniente) da peça
![referencial central](img/20200221-referencial_central.svg)

Este furo deverá ter o diâmetro `Ø12H7` e ter uma chanfragem de 2mm de modo a proteger a superfície e arestas do cilindro, protegendo o rigor dessa marcação.
Pode-se usar uma das paredes brutas como orientação, desde que as maquinações possam ser executadas dentro do material.
