2020-02-12

# Colunas e fixação
A fixação das colunas ao tecto ou sobretecto deve seguir as indicações do fabricante (ex: Nitrogas).
Por exemplo, umas das indicações diz que o diâmetro da caixa do cilindro tem uma medida igual ao diâmetro do cilindro mais uma tolerância (+1.0/+0.5). Esta tolerância é apertada o suficiente para evitar que o cilindro torça conforme faz força numa superfície inclinada.
