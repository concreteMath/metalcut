2020-01-06

# Ficheiros para jacto de água
Os ficheiros para corte de jacto de água servem para enviar para a empresa que vai efectuar o corte.

Começa-se por identificar as peças que têm o material certo para o corte.
Vai-s à lista de materiais (caixa "Assembly Manager" > "Parts list view" > "Table") e identificam-se todas as peças que tenham o material "Chapa Azul" (1.0601 ou M7(sistema Ramada)) e com espessura menor que 15/16mm.
Como alternativa, pode-se ordenar a lista por "Material" e exportar como CSV usando o botão "Export table".
No excel, pode-se filtrar os items de interesse.

Em seguida, agrupam-se as peças pela espessura de chapa de onde vão ser cortadas. Normalmente, as espessuras das peças têm 0.5mm a menos que da espessura de chapa de onde vão ser cortadas. Exemplo: peça com 5.5mm vai ser cortada de uma chapa de 6mm.

Para cada espessura de chapa, cria-se um desenho de acordo com o descrito no ficheiro "20200102-jacto_de_agua-placas_de_choque.md"

Eu costumo enquadrar cada composição de chapas e etiqueta dentro de um rectângulo com medidas standard A_x (A4, A3, etc...)

Cada folha deve ser exportada para um ficheiro individual DXF.
A pasta onde se guardam o ficheiros é:
`\\pjfserver\E\2.MEGADIES\3.DESENVOLVIMENTO\2.DOSSIER DE PRODUÇÃO\OBRAS\$ANO\$OBRA\subcontrato\JAgua\$data_hoje`
em que
`$ANO` é o ano da ferramenta (ex 2019)
`$OBRA` é o código da ferramenta (ex 1901015)
`$data_hoje` é a data de hoje (ex 2020_01_06)

Cada DXF poderá ter o nome
`${desc}_${Ref}__${esp}mm.dxf`
em que
`$desc` é a descrição do tipo de peças (ex "placa_choque")
`$ref` é a referência interna das peças, seja uma ou um intervalo (ex C1102, A5302-05)
`$esp` é a espessura da chapa a cortar (ex 10)
