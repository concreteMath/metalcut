2020-01-09

# Detalhe - Porta Pilotos - tolerâncias
A altura (na direcção Z) deverá ter a tolerância x(+/-0.02)
![porta pilotos tolerancia z](img/20200109-porta_pilotos_tolerancia_z.svg)

Dada a existência de pilotos, a distância entre eles tem de ser tolerânciada com x(+/-0.02).
![porta pilotos toleracia pilotos](img/20200109-porta_pilotos_tolerancia_pilotos.svg)

Os pilotos e as cavilhas têm tolerância H7.
