2019-12-17

# Detalhe Lower Die
- Normalmente, começas-e por detalhar as peças pequenas, confirmando as furações na base. Só no fim é que se detalha a própria base.
- Meter indicação das cavilhas
- Os elementos que têm casquilho levam uma indicação do diâmetro e de tolerância geométrica
![casquilho corte](img/20191217-casquilho_corte.svg)
- Meter planos de referência

## Caixas
- Adicionar cotagens toleranciadas entre faces que obriguem a uma dada compensação como por exemplo H7. (METER FIGURA DE EXEMPLO DE CAIXA)

## Corte do Lower Die
- O corte deve passar em zonas que possam mostrar informação vertical que é difícil de mostrar na vista de cima.
- A linha de corte ao passar numa zona cilíndrica, deve passar no centro do círculo.

## Tolerâncias
- Zonas de suporte de matrizes de forma devem ser toleranciadas com x+/-0.02, sendo x a altura desde o plano de referência da base (em baixo) até à zona de suporte.
