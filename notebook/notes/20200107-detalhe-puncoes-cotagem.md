2020-01-07

# Detalhe - Punções - cotagem
Embora o desenho de um punção tenha bastantes pormenores, a cotagem é bastante simplificada.
![detalhe puncoes cotagem](img/detalhe_puncoes_cotagem.svg)

Basta indicar a largura e comprimento totais (H e W), a altura (L), a altura do dente (D), o tamanho e o tipo de furações.
A largura H poderá ser indicada entre parêntesis, indicando que poderá ser uma medida antiga ou que a distância física correspondente pode mudar. Isso deve-se por exemplo à procuração de "trims", onde o constante ajuste dos punções faz variar imenso a largura do punção.
