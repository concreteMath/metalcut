2019-12-20

# Matriz de corte
Uma matriz de corte tem uma (ou mais) cavidade onde entre um punção de corte.
Se a cavidade de corte tiver uma face de reacção, pode-se dar uma tolerância a essa face, tal que vá do nominal e entre no materal até 0.02mm. Pelo menos ter uma indicação de que é uma face de encosto com o simbolo:
![simbolo face de encosto](img/20191220-simbolo_face_encosto.svg)

Para executar o corte inclinado, é necessário indicar alguns parâmetros numa caixa:
![caixa parametros corte inclinado](img/20181220-caixa_parametros_corte_inclinado.svg)

onde c é a espessura da chapa, L é a altura de vida e A é ângulo de saída.
Os parâmetros L e A terão de ser de acordo como o caderno de encargos.

## Furo de enfiamento
Parâmetros:
- Círculo de diâmetro 6mm, com centro distanciado 6mm da maior face direita, preferencialmente de enconsto.

## Sucata a cortar no fio
- Representar com um hatch a 45º de quadrículas com 5mm de lado. Reduzir o espaçamento da quadrícula, caso a zona de sucata seja muito pequena.
