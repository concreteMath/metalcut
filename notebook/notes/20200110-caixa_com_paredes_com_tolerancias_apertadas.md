2020-01-10

# Caixa com paredes com tolerâncias apertadas
![simbolo caixa toleranciada](img/20200110-simbolo_caixa_toleranciada.svg)
As caixas assinaladas com o símbolo de caixa toleranciada podem ser suficientes pra travar o movimento da peça que lá esteja encastrada.
Caso um dos eixos seja livre, pode-se meter uma cavilha.
