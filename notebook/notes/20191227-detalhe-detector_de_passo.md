2019-12-27

# Detector de Passo
O linguete do detector de passo corre dentro da régua. Dá-se a tolerância H7 à cavidade da régua e ao linguete dá-se a tolerância g6.
Embora seja uma tolerância apertada, se for necessário, na bancada pode-se rectificar a passagem.
![passagem linguete na regua](img/passagem_linguete_na_regua.svg)
