2020-01-14

# Alteração na lista de materiais
Caso haja alguma alteração da posição, referência interna, material, dimensões, etc... será necessário primeiro alterar essas características no modelo 3D.
Caso essa peça já tenha sido lançada na lista de materiais do SINEX, há que primeiro pedir ao responsável das compras (João) para devolver as linhas (entradas das peças no SINEX da ferramenta) para que os desenhadores/projectistas possam introduzir as alterações no Sinex.
