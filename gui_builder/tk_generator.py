#TODO Provavelmente, a melhor abordagem será criar objectos para cada elemento??

rootWindow_name = "root"

def head():
	return "#!/usr/bin/python3\n"\
	+ "import tkinter as tk\n" + rootWindow_name\
	+ " = tk.Tk()\n"

def foot():
	return rootWindow_name + ".mainloop()\n"

# Add generic properties
def element_generic(element_type, master, index, pos_i, pos_j):
	name = element_type.lower() + "_" + str(index)
	code = name + " = tk." + element_type + "(" + master + ")\n"\
	       + name + ".grid(row = " + str(pos_i) + ", column = " +  str(pos_j)\
		   + ")\n"
	return code

#TODO talvez arranjar um contador de elementos
#TODO estou a ver muito código repetido
#FIXME o nome dos elementos está a ser criado em vários pontos. Deveria ser num
#      só sítio
def element_button(item, master, index, pos_i, pos_j):
	name = "button_" + str(index)
	code = element_generic("Button", master, index, pos_i, pos_j)
	for k in item[1]:
		v = item[1][k]
		if k == "t":
			code += name + ".config(text = \"" + v + "\")\n"
	return code

def element_label(item, master, index, pos_i, pos_j):
	name = "label_" + str(index)
	code = element_generic("Label", master, index, pos_i, pos_j)
	for k in item[1]:
		v = item[1][k]
		if k == "t":
			code += name + ".config(text = \"" + v + "\")\n"
	return code

def element_entry(item, master, index, pos_i, pos_j):
	code = element_generic("Entry", master, index, pos_i, pos_j)
	return code

def codeGen(schema):
	txt = []
	txt.append(head())
	item_count = 0
	for i in range(len(schema)):
		for j in range(len(schema[i])):
			item = schema[i][j]
			i_type = item[0]
			if i_type == 'B':
				txt.append(\
				element_button(item, rootWindow_name, item_count, i, j))
			elif i_type == 'L':
				txt.append(\
				element_label(item, rootWindow_name, item_count, i, j))
			elif i_type == 'E':
				txt.append(\
				element_entry(item, rootWindow_name, item_count, i, j))
			else:
				txt.append("# item:" + item + "\n")
			item_count += 1
	txt.append(foot())
	code = "".join(txt)
	return code
