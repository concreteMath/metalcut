#!/usr/bin/python3

import sys
import tk_generator as tkgen

def schema_parse_item(txt):
	item = [txt[0], {}]
	# Jump 1 char, the item type identifier
	i0 = txt.find('{', 1)
	i1 = txt.find('}', 1)
	if i0 == -1 or i1 == -1:
		return item
	dataTxt = txt[i0+1:i1]
	pairs = dataTxt.split(',')
	for p in pairs:
		k, v = p.split(':')
		k = k.strip()
		v = v.strip()
		item[1][k] = v
	return item

def schema_parse(txt):
	schema = []
	for row in txt.split("\n"):
		if row == "":
			continue
		items = row.strip().split()
		items_row = []
		for itemTxt in items:
			items_row.append(schema_parse_item(itemTxt))
		schema.append(items_row)
	return schema

def textFile_read(fn):
	fp = open(fn, "r")
	contents = fp.read()
	fp.close()
	return contents

def textFile_write(fn, text):
	fp = open(fn, "w")
	fp.write(text)
	fp.close()


fn  = sys.argv[1]
txt = textFile_read(fn)

schema = schema_parse(txt)
print(schema)
code   = tkgen.codeGen(schema)
print(code)
textFile_write("app.pyw", code)
print(schema)
