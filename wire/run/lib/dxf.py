import sys
from lib.geo import *
from lib.math import *


def dxf_read(fn):
	fp = open(fn, "r", encoding = "latin1")
	lines = fp.read().splitlines()
	fp.close()
	return lines




# Tentar respeitar os nomes do spec do dxf
# Penso que cada key tenha um tipo definido, portanto, poderia ter uma tabela
# de funções de parsing já preparada
# Os items de parsing devem ir parar a uma tabela (bloco de memória), onde o seu
# acesso deve ser na base de um ponteiro.

# Vou sacar os pares (key, value)
# Estou a assumir que existem sempre pares de linhas key, value
# TODO: verificar o número de linhas e se é possível extrair mais linhas
def get_entity(lines, i0, items):
	items_len = len(items)
	data = [None]*items_len
	i = i0
	while True:
		i += 2
		if lines[i] == "  0":
			break
		if lines[i] in items:
			j = items.index(lines[i])
			data[j] = lines[i+1]
	return data


def get_line(lines, i0):
	#c, x0, y0, x1, y1
	items = [" 62", " 10", " 20", " 11", " 21"]
	data  = get_entity(lines, i0, items)
	line = geo_line()
	line.color = int(data[0])
	line.p0    = [float(data[1]), float(data[2]), 0.0]
	line.p1    = [float(data[3]), float(data[4]), 0.0]
	return line


def get_arc(lines, i0):
	#c, x0, y0, r, a0, a1
	items = [" 62", " 10", " 20", " 40", " 50", " 51"]
	data  = get_entity(lines, i0, items)
	arc = geo_arc()
	arc.color = int(data[0])
	arc.center = [float(data[1]), float(data[2]), 0.0]
	arc.radius = float(data[3])
	arc.angle0 = float(data[4])
	arc.angle1 = float(data[5])
	return arc


def get_circle(lines, i0):
	#c, x0, y0, r
	items = [" 62", " 10", " 20", " 40"]
	data  = get_entity(lines, i0, items)
	circle = geo_circle()
	circle.color  = int(data[0])
	circle.center = [float(data[1]), float(data[2]), 0.0]
	circle.radius = float(data[3])
	return circle


def get_text(lines, i0):
	# c, x0, y0, text
	items = [" 62", " 10", " 20", "  1"]
	data = get_entity(lines, i0, items)
	text = geo_text()
	text.color = int(data[0])
	text.pos   = [float(data[1]), float(data[2]), 0.0]
	text.text  = data[3]
	return text


def write_dxf(fn, geo):
	head=\
	"""  0
SECTION
  2
ENTITIES
"""

	foot=\
	"""  0
ENDSEC
  0
EOF
"""
	fp = open(fn, "w", encoding="latin1")
	
	fp.write(head)
	
	#FIXME: talvez seja necessário identar as keys com zeros
	#TODO: Talvez possa abstrair o bulk do código
	for obj in geo["lines"]:
		fp.write("  0\nLINE\n")
		fp.write("100\nAcDbEntity\n")
		fp.write("  8\n0\n")
		fp.write("100\nAcDbLine\n")
		fp.write(" 10\n%.15g\n"%obj[1])
		fp.write(" 20\n%.15g\n"%obj[2])
		fp.write(" 30\n0.0\n")
		fp.write(" 11\n%.15g\n"%obj[3])
		fp.write(" 21\n%.15g\n"%obj[4])
		fp.write(" 31\n0.0\n")
	
	for obj in geo["circles"]:
		fp.write("  0\nCIRCLE\n")
		fp.write("100\nAcDbEntity\n")
		fp.write("  8\n0\n")
		fp.write("100\nAcDbCircle\n")
		fp.write(" 10\n%.15g\n"%obj[1])
		fp.write(" 20\n%.15g\n"%obj[2])
		fp.write(" 30\n0.0\n")
		fp.write(" 40\n%.15g\n"%obj[3])
	
	for obj in geo["arcs"]:
		fp.write("  0\nARC\n")
		fp.write("100\nAcDbEntity\n")
		fp.write("  8\n0\n")
		fp.write("100\nAcDbCircle\n")
		fp.write(" 10\n%.15g\n"%obj[1])
		fp.write(" 20\n%.15g\n"%obj[2])
		fp.write(" 30\n0.0\n")
		fp.write(" 40\n%.15g\n"%obj[3])
		fp.write("100\nAcDbArc\n")
		fp.write(" 50\n%.15g\n"%obj[4])
		fp.write(" 51\n%.15g\n"%obj[5])

	fp.write(foot)
	
	fp.close()



# FIXME Eu não sei se a estrutura de dados para a geometria são dicionários de
# listas de túpulos, mas por enquanto vai assim.
# Acho que o que vai ditar a estruturação dos dados vai ser a forma como esta
# vai ser processada? (a regra não devia ser adaptar os algoritmos aos dados, e
# não o inverso?)

def get_lines(flines):
	i = 0

	# FIXME estes deviam ser dicionarios, em que cada entrada é uma propriedade
	# da entidade, ou seja, cada propriedade é uma lista.
	# Por enquanto vão ser listas de túpulos.
	lines   = []
	circles = []
	arcs    = []

	while i < len(flines):
		l = flines[i]
		if l != '  0':
			i+=1
			continue
		entity_type = flines[i+1]
		#print(entity_type)

		if entity_type == "LINE":
			lines.append(get_line(flines, i))
		elif entity_type == "CIRCLE":
			circles.append(get_circle(flines, i))
		elif entity_type == "ARC":
			arcs.append(get_arc(flines, i))

		i+=1
	
	geo = {"lines": lines, "circles": circles, "arcs": arcs}

	return geo


def get_blocks(flines):
	geo_noblock = []

	blocks = []
	
	insideBlock = False
	
	for i in range(len(flines)):
		l = flines[i]
		if l != '  0':
			continue

		entity_type = flines[i+1]

		if entity_type == "BLOCK":
			geo_block = []
			insideBlock = True
			continue
		elif entity_type == "ENDBLK":
			blocks.append(geo_block)
			insideBlock = False
			continue

		if insideBlock:
			geo_ptr = geo_block
		else:
			geo_ptr = geo_noblock

		if entity_type == "LINE":
			geo_ptr.append(get_line(flines, i))
		elif entity_type == "CIRCLE":
			geo_ptr.append(get_circle(flines, i))
		elif entity_type == "ARC":
			geo_ptr.append(get_arc(flines, i))
		elif entity_type == "TEXT":
			geo_ptr.append(get_text(flines, i))
	
	return [geo_noblock] + blocks
