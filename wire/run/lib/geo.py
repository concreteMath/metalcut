# Geometry library

from math import pi, sin, cos

class geo_line:
	def __init__(self):
		self.type  = "LINE"
		self.color = 0
		self.p0    = [0.0, 0.0, 0.0]
		self.p1    = [0.0, 0.0, 0.0]
	
	def endpoints(self):
		return (self.p0, self.p1)


class geo_circle:
	def __init__(self):
		self.type   = "CIRCLE"
		self.color  = 0
		self.center = [0.0, 0.0, 0.0]
		self.radius = 0.0
	
	def endpoints(self):
		return None


class geo_arc:
	def __init__(self):
		self.type   = "ARC"
		self.color  = 0
		self.center = [0.0,0.0,0.0]
		self.radius = 0.0
		self.angle0 = 0.0
		self.angle1 = 0.0
	def get_endpoints(self):
		return (\
			[self.center[0] + self.radius*cos(self.angle0/180.0*pi),
			 self.center[1] + self.radius*sin(self.angle0/180.0*pi),
			 self.center[2]],
			[self.center[0] + self.radius*cos(self.angle1/180.0*pi),
			 self.center[1] + self.radius*sin(self.angle1/180.0*pi),
			 self.center[2]])


class geo_text:
	def __init__(self):
		self.type  = "TEXT"
		self.color = 0
		self.pos   = [0.0, 0.0, 0.0]
		self.text  = ""
