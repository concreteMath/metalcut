from math import pi, cos, sin, acos, asin
from lib.math import *
from lib.geo import *


# Separate the dxf drawing items by color
# FIXME
# Isto deveria ser aprendido e não explicitamente posto, isto é, o script
# deveria descobrir como os dados estão estruturados?

def separate_by_color(geo):
	# NAO USAR ISTO DIRECTAMENTE
	# Isto é apenas uma indicação
	
	# LEGENDA À PRIÓRI
	#0  : "-",
	#2  : "legend",
	#4  : "piece_metal",
	#7  : "-",
	#30 : "section_inside",
	#56 : "medidas",
	#80 : "invisible_line_maybe",
	#96 : "paper",
	#100: "section_lines", 
	#136: "cavilha",
	#132: "-",
	#156: "-",

	parts = {}

	for item in geo:
		color = item.color
		if color not in parts:
			parts[color] = []
		parts[color].append(item)
	
	return parts




#XXX
# Isto está a usar assunções erradas. O metal pode ter uma cor diferente.
def find_cavilhas(parts):
	# tolerância
	# décima de micron
	tol = 0.0001

	cav   = parts[136]
	metal = parts[4]
	
	# Vou procurar o contentor minimo de cada cavilha

	# Find container bounding boxes
	bbox = []
	for item in metal["circles"]:
		cx = item[1]
		cy = item[2]
		r  = item[3]
		xmin = cx - r - tol
		ymin = cy - r - tol
		xmax = cx + r + tol
		ymax = cy + r + tol
		
		bbox.append([xmin, ymin, xmax, ymax])
	
	# Find container bounding box
	bbox_container = [-1]*len(cav["lines"])
	for i in range(len(cav["lines"])):
		l = cav["lines"][i]
		x0 = l[1]
		y0 = l[2]
		x1 = l[3]
		y1 = l[4]

		xmin = min(x0,x1)
		xmax = max(x0,x1)
		ymin = min(y0,y1)
		ymax = max(y0,y1)
		
		# Vou procurar pela bounding box minima que contenha o item
		bbox_min = [xmin-1000, ymin-1000, xmax+1000, ymax+1000]
		for j in range(len(bbox)):
			bb = bbox[j]
			if xmin < bb[0] or ymin < bb[1] or xmax > bb[2] or ymax > bb[3]:
				continue
			
			# check if bounding box fits inside the found bonding box
			if bbox_min[0] < bb[0] and bbox_min[1] < bb[1] and\
			   bbox_min[2] > bb[2] and bbox_min[3] > bb[3]:
				bbox_container[i] = j
	

	bbox_selected = set(bbox_container)

	geo_out = {"lines":[], "circles":[], "arcs":[]}
	for b in bbox_selected:
		if b == -1:
			continue
		geo_out["circles"].append(metal["circles"][b])
	
	return geo_out




def decode_cav_text(text):
	i0 = 0
	for i in range(len(text)):
		if not text[i].isdigit():
			cav_size = int(text[i0:i])
			i0 = i
			break
	tol_code = text[i0]
	tol_num = int(text[i0+1:])
	return (cav_size, tol_code, tol_num)




# O que é um block de uma cavilha?
# A descrição tem que ser tal que eu consiga reconstruir uma cavilha sem mais
# nenhuma ajuda. Quase que vou precisar de um programa de fazer desenhos de
# cavilhas para identificar uma delas.
# Uma linha horizontal verde clara (cor 100)
# Várias linhas (verde escuras (cor 136) a 45graus em dois quadrantes.
def match_cavilha(geo):
	# tolerance of 1 nanometer
	tol = 1e-6
	
	# All members must be lines except a single text item
	n_text = 1
	for item in geo:
		if item.type == "LINE":
			continue
		elif item.type == "TEXT":
			n_text -= 1
			if n_text < 0:
				return False
		else:
			return False

	# Separate by color
	parts = separate_by_color(geo)

	# It has 2 colors
	if len(parts) != 2:
		return False
	
	# It has the colors 100 and 136
	if 100 not in parts or 136 not in parts:
		return False
	
	# The horizontal line defines the four quadrants
	if len(parts[100]) != 2:
		return False
	
	# Find the line of color 100
	block_text = ""
	for item in parts[100]:
		if item.type == "LINE":
			hline = item
		elif item.type == "TEXT":
			block_text = item.text
	
	# Decode the text's information
	block_text_info = decode_cav_text(block_text)

	# The horizontal line must be horizontal
	if abs(hline.p0[1] - hline.p1[1]) >= tol:
		return False

	# Prova que os segmentos estão a +/-45 graus
	for l in parts[136]:
		dx = l.p1[0] - l.p0[0]
		dy = l.p1[1] - l.p0[1]
		if abs(abs(dx)-abs(dy)) >= tol:
			return False
	
	#TODO Falta avaliar a relação entre as linhas
	
	return True




def cavilha_to_circle(geo):
	# tolerance of 1 nanometer
	tol = 1e-6

	#TODO provavelmente deveria verificar se a geometria de input é uma cavilha
	found_hline = False

	parts = separate_by_color(geo)

	if 100 not in parts:
		return None

	circle = geo_circle()
	
	hasCenter = False
	hasRadius = False
	for item in parts[100]:
		# I'm assuming this is the green horizontal line
		# FIXME talvez isto devesse ser mais robusto e não assumir a
		# horizontalidade
		if item.type == "LINE":
			circle.center = [(item.p0[0] + item.p1[0])/2.0, item.p0[1], 0.0]
			hasCenter = True
		elif item.type == "TEXT":
			info = decode_cav_text(item.text)
			circle.radius = info[0]/2.0
			hasRadius = True
	
	if not (hasCenter and hasRadius):
		return None
	
	return circle




def match_cut_hatch(geo):
	tol = 1e-6

	parts = separate_by_color(geo)
	print(parts.keys())

	if len(parts) != 1:
		return False
	
	if 136 not in parts:
		return False

	lines = parts[136]
	for l in lines:
		dx = l.p1[0] - l.p0[0]
		dy = l.p1[1] - l.p0[1]
		if abs(abs(dx)-abs(dy)) >= tol:
			return False

	# Falta avaliar a relação entre as linhas
		
	print("cut hatch:", parts.keys())
	return True




# TODO Optimize brute-force approach
def cut_hatch_border_points(hatch):
	tol  = 1e-6
	tol2 = 1e-6**2.0
	# Border points belong to just one line
	points = []
	for l in hatch:
		points += list(l.endpoints())
	
	unique_points = []
	for i in range(len(points)):
		xi = points[i][0]
		yi = points[i][1]
		zi = points[i][2]
		unique_flag = True
		for j in range(len(points)):
			if i==j:
				continue

			xj = points[j][0]
			yj = points[j][1]
			zj = points[j][2]
			adx = abs(xj-xi)
			ady = abs(yj-yi)
			adz = abs(zj-zi)

			if adx > tol or ady > tol or adz > tol:
				continue
			
			if adx**2.0 + ady**2.0 + adz**2.0 <= tol2:
				unique_flag = False
				break
		
		if unique_flag:
			unique_points.append(points[i])
			
	return unique_points




def find_cavilhas_2(blocks):
	geo_out = []
	for b in blocks:
		if not match_cavilha(b):
			continue
		circle = cavilha_to_circle(b)
		if circle == None:
			continue
		else:
			geo_out.append(circle)
	return geo_out




def make_pescocos(geo_in):
	meia_largura = 0.6
	r_extra      = 0.5

	geo_out = []
	for item in geo_in:

		if item.type != "CIRCLE":
			continue

		cx = item.center[0]
		cy = item.center[1]
		r0 = item.radius

		r1 = r0 + r_extra
		
		# Intersection angle calculation
		dt0 = asin(0.6/r0)
		dt1 = asin(0.6/r1)
		print(dt0, dt1)
		
		# Inside angles
		t00 = pi/4 - dt0
		t01 = pi/4 + dt0
		# Outside angles
		t10 = pi/4 - dt1
		t11 = pi/4 + dt1

		p00 = [cx + r0*cos(t00), cy + r0*sin(t00), 0.0]
		p01 = [cx + r0*cos(t01), cy + r0*sin(t01), 0.0]
		p10 = [cx + r1*cos(t10), cy + r1*sin(t10), 0.0]
		p11 = [cx + r1*cos(t11), cy + r1*sin(t11), 0.0]
		p1mid = [(p10[0] + p11[0])/2.0, (p10[1] + p11[1])/2.0, 0.0]

		line0 = geo_line()
		line1 = geo_line()
		line2 = geo_line()
		line3 = geo_line()

		line0.p0 = p1mid
		line0.p1 = p11
		line1.p0 = p11
		line1.p1 = p01
		line2.p0 = p00
		line2.p1 = p10
		line3.p0 = p10
		line3.p1 = p1mid

		arc = geo_arc()
		arc.center = [cx, cy]
		arc.radius = r0
		arc.angle0 = t01*180/pi
		arc.angle1 = t00*180/pi

		geo_out.append(line0)
		geo_out.append(line1)
		geo_out.append(arc)
		geo_out.append(line2)
		geo_out.append(line3)
	
	return geo_out




def get_connect_items(geo):
	tol = 1e-4
	tol2 = tol**2.0
	
	# BRUTE ALGORITHM
	# Extract endpoints
	# Find which endpoints coincide (build graph)
	# If more than 2 segments coincide, ignore.
	# Extract groups from graph

	endpoints = []
	groups = []

	# Determinar o numero de endpoints
	n_endpoints = 0
	for item in geo:
		if item.type == "LINE" or item.type == "ARC":
			n_endpoints += 2
	groups = [None] * n_endpoints

	for i in range(len(geo)):
		item = geo[i]
		
		if item.type == "LINE":
			endpoints.append([item.p0[0], item.p0[1], i])
			endpoints.append([item.p1[0], item.p1[1], i])
		elif item.type == "ARC":
			#cx = item.center[0]
			#cy = item.center[1]
			#r  = item.radius
			#a0 = item.angle0
			#a1 = item.angle1
			#endpoints.append([cx + r*cosd(a0), cy + r*sind(a0), i])
			#endpoints.append([cx + r*cosd(a1), cy + r*sind(a1), i])
			end_pt = item.get_endpoints()
			endpoints.append([end_pt[0][0], end_pt[0][1], i])
			endpoints.append([end_pt[1][0], end_pt[1][1], i])
		else:
			continue

	#BRUTE FORCE
	
	# Construção da rede de ligações
	viz = []
	for i in range(len(geo)):
		viz.append([])
	
	for i in range(n_endpoints):
		ei = endpoints[i]
		item_i = ei[2]

		for j in range(n_endpoints):
			if j == i:
				continue
			ej = endpoints[j]
			d2 = (ej[0] - ei[0])**2.0 + (ej[1] - ei[1])**2.0
			if d2 >= tol2:
				continue

			item_j = ej[2]

			viz[item_i].append(item_j)
	
	# Construção dos grupos
	groups = []
	done = [False]*len(geo)

	# Breadth first search
	while True:
		# Search non processed item
		all_done = True
		for i in range(len(geo)):
			if done[i]:
				continue
			elif len(viz[i]) == 0:
				groups.append([geo[i]])
				done[i] = True
			else:
				all_done = False
				fifo = [i]
				done[i] = True
				break

		if all_done:
			break
		
		# Build group
		group = []
		while len(fifo) > 0:
			x = fifo.pop(0)
			group.append(geo[x])
			for y in viz[x]:
				if done[y]:
					continue
				else:
					fifo.append(y)
					done[y] = True

		groups.append(group)
	
	return groups




def get_cut_blocks(blocks):
	out = []
	for b in blocks:
		if match_cut_hatch(b):
			out.append(b)
	return out
