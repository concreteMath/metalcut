# TODO Criar um criador de TRAV, o qual determina que eixos se devem mover
# TODO Verificar que tipo de números (double, single) come o código da KE.
#      (talvez possa fazer uma análise do código gerado)
#      (creio que a precisão vai até aos microns)

def code_cut_offset(off):
	code = []
	if off.tech != None:
		code.append("LOAD TECH " + off.tech)
	code.append("ABS")
	code.append("METR")
	if off.offset != None:
		code.append("OFFSET" + str(off.offset))
	code.append("TRAV X0 Y0 U0 V0 Z0")
	#FIXME verificar de onde vem este THICK
	code.append("COOR X0 Y0 U0 V0 Z0 THICK 10.")
	return code
		

# FIXME Falta o ponto actual
def code_cut_line(l):
	x1 = l.p1[0]
	y1 = l.p1[1]
	return ["INTL X%.3g Y%.3g"%(x1, y1)]


def code_cut_arc(a):
	#arc_angle = (a.angle1 - a.angle0)%360.0
	#if arc_angle > 180


def code_cut_path(path):
	code = []

	if len(path.path) == 0:
		return code
	
	code.append("TRAV X%.3g Y%.3g"%(path.entry[0], path.entry[1]))
	code.append("TECH THICK %.3g CRIT 8"%path.thick)
	code.append("AWF")
	if path.toff != None:
		code.append("TOFF %d"%path.toff)
	if path.feed != None:
		code.append("FEED %d"%path.feed)
	#Compensações

	# Ponto inicial
	#FIXME faltam os "endpoints" do circulo
	#FIXME Talvez tenha de banir circulos
	endpoints = path[0].endpoint()

	#FIXME falta o código das compensações
	for item in path.path:
		if item.type == "LINE":
			code += code_cut_line(item)
		elif item.type == "ARC":
			code += code_cut_arc(item)
	


def cut_sequence_to_code(cut_seq):
	code = []
	current_offset = None
	for cut in cut_seq:
		if cut.type == "OFFSET":
			current_offset = cut
			code += code_cut_offset(cut)
		elif cut.type == "PATH":
			

			
