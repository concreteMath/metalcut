from math import cos, sin 

piover180 = 0.017453292519943295

def cosd(a):
	return cos(a*piover180)


def sind(a):
	return sin(a*piover180)
