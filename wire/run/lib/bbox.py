class Bbox:
	_init_():
		self.x0 = 0.0
		self.x1 = 0.0
		self.y0 = 0.0
		self.y1 = 0.0

#FIXME fazer para 3d
def intersect(bb0, bb1):
	bb2 = Bbox()
	x0 = max(bb0.x0, bb1.x0)
	x1 = min(bb0.x1, bb1.x1)
	if x0 > x1:
		return bb2
	y0 = max(bb0.y0, bb1.y0)
	y1 = min(bb0.y1, bb1.y1)
	if y0 > y1:
		return bb2
	
	bb2.x0 = x0
	bb2.x1 = x1
	bb2.y0 = y0
	bb2.y1 = y1

	return bb2
