# FIXME confirmar nomes dos parâmetros
# Consultar Manual da KE e o ISO

# No caso de ter várias passagens, posso simplesmente copiar um cut e mexer
# em alguns parâmetros

class cut_path:
	def __init__(self):
		self.type = "CUT"
		self.path = []
		#FIXME preciso de nomes melhores
		self.entry = [0.0, 0.0, 0.0]
		self.exit  = [0.0, 0.0, 0.0]
		#FIXME isto provavelmente será configurado exteriormente.
		# Aliás, a compensação é reflexo da tolerância exigida.
		self.tolerance = 0.0
		self.height   = 0.0
		# Parâmetros da compensação
		self.comp     = 0.0
		# Parâmetros do corte
		self.feed     = None
		self.wirefeed = None
		self.pause    = None

class cut_offset:
	def __init__(self):
		self.type = "OFFSET"
		pos = [0.0, 0.0, 0.0]
		offset = [0.0, 0.0, 0.0]
		tech = ""
