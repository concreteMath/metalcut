#!/usr/bin/python3

import sys
import os
from lib.dxf import *
from lib.project import *
from lib.svg import *

if len(sys.argv) != 2:
	print("usage: make_pescocos.py FILENAME")
	exit(2)

fn = sys.argv[1]

if not os.path.isfile(fn):
	print(fn, ": file not found")
	exit(2)

lines    = dxf_read(fn)
blocks   = get_blocks(lines)
#connected_groups = get_connect_items(blocks[0])
cutting_blocks = get_cut_blocks(blocks)
for p in cut_hatch_border_points(cutting_blocks[0]):
	print(p)

save_geo_groups("out/cut0", 500, 500, cutting_blocks)
