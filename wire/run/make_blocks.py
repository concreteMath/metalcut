#!/usr/bin/python3

import sys
import os
from lib.dxf import *
from lib.project import *
from lib.svg import *

if len(sys.argv) != 2:
	print("usage: make_pescocos.py FILENAME")
	exit(2)

print(sys.argv)
fn = sys.argv[1]

if not os.path.isfile(fn):
	print(fn, ": file not found")
	exit(2)

lines  = dxf_read(fn)
blocks = get_blocks(lines)
save_geo_blocks("out/blocks", 500, 500, blocks)
