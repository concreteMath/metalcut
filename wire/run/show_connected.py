#!/usr/bin/python3

import sys
import os
from lib.dxf import *
from lib.project import *

if len(sys.argv) != 2:
	print("usage: make_pescocos.py FILENAME")
	exit(2)

fn = sys.argv[1]

if not os.path.isfile(fn):
	print(fn, ": file not found")
	exit(2)

lines    = dxf_read(fn)
geo      = get_lines(lines)

parts    = separate_entities(geo)
connect_segments(parts[4])


#cavilhas = find_cavilhas(parts)
#pescocos = make_pescocos(cavilhas)

#write_dxf("pescocos.dxf", pescocos)
