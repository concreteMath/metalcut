import pjf_dim_matriz_puncao as mp

def test_0():
	print(mp.calc_diePunch_diam(  2,  15, -0.05, 0.2))
	print(mp.calc_diePunch_diam(  2,  15, -0.5 , 0.5))
	print(mp.calc_diePunch_diam(  2, 5.2, -0.05, 0.2))
	print(mp.calc_diePunch_diam(  2,   8, -0.05, 0.2))
	print(mp.calc_diePunch_diam(1.2, 5.2, -0.05, 0.2))
	print(mp.calc_diePunch_diam(1.2,   8, -0.05, 0.2))

# Test excel round
def test_1():
	print(mp.excel_round( 1.1))
	print(mp.excel_round(-1.1))
	print(mp.excel_round( 1.5))
	print(mp.excel_round(-1.5))
	print(mp.excel_round( 1.573, 1))
	print(mp.excel_round(-1.573, 1))
	print(mp.excel_round( 5.000, -1))
	print(mp.excel_round(-1.573, 0))
	print(mp.excel_round( 19.000, -1))

test_1()