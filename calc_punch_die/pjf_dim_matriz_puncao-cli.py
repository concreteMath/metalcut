# PJF lib
# Punch and hole dimensioning

import sys
import pjf_dim_matriz_puncao as mp

def usage():
	print("usage: pjf_dim_matriz_puncao-cli.py THICK HOLE_D TOLMIN TOLMAX")


if len(sys.argv) != 5:
	usage()
	exit(2)

thick   = float(sys.argv[1])
hole_d  = float(sys.argv[2])
tol_min = float(sys.argv[3])
tol_max = float(sys.argv[4])

res = calc_diePunch_diam(thick, hole_d, tol_min, tol_max)
print("punch:", res["punch"], "\t", "die:", res["die"])
