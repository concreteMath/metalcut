# PJF lib
# Punch and hole dimensioning

# O round do python segue a norma "round to even".
# A fórmula seguinte recria o arredondamento do excel ("away from 0")
def excel_round(x, n=0):
	p = 10.0**n
	return int(x*p  + (x>=0) - 0.5) / p


# Cálculo segundo a gestamp
# Referências:
# Manual da gestamp
# gap_factor: pág.70 sec.12.4
# punch_d: pág.65 sec 10.1.2
# die_d: pág.70 sec 12.4
def calc_diePunch_diam(thick, hole_d, tol_min, tol_max):
	# Estou a usar os parâmetros da gestamp
	gap_factor = 0.1
	
	punch_d = hole_d + tol_min + 0.75*(tol_max - tol_min)
	punch_d = excel_round(punch_d, 1)
	
	die_d  = punch_d + 2.0*gap_factor*thick
	return {"punch": punch_d, "die": die_d}