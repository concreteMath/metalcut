import sys
import os
import csv

#TODO
# - Output para ficheiro em utf8

class Piece:
    def __init__(self):
        self.pos = None
        self.ref = None
        self.mat = None
        self.quant   = None
        self.dimRect = False
        self.dimCil  = False
        self.dimx = None
        self.dimy = None
        self.dimz = None
        self.dimd = None
    
    def print(self):
        print(self.pos, self.ref, self.mat, self.quant, self.dimRect,\
              self.dimCil, self.dimx, self.dimy, self.dimz, self.dimd,\
              sep="\t")
    
    def __eq__(self, other):
        out = True
        out &= self.pos     == other.pos
        out &= self.ref     == other.ref
        out &= self.mat     == other.mat
        out &= self.quant   == other.quant
        out &= self.dimRect == other.dimRect
        out &= self.dimCil  == other.dimCil
        out &= self.dimx    == other.dimx
        out &= self.dimy    == other.dimy
        out &= self.dimz    == other.dimz
        out &= self.dimd    == other.dimd
        return out



def csv_read_data(csv_filename, encoding="utf_8", delimiter=","):
    if not os.path.isfile(csv_filename):
        print(csv_filename, "not found")
        return None
    data = []
    csv_fp = open(csv_filename, "r", encoding=encoding)
    csv_data = csv.reader(csv_fp, delimiter=delimiter)
    
    return [row for row in csv_data]


# TODO: Fazer pretty print de comparações de peças
def csv_model2canonical(data):
    # Ignore header
    out = []
    for i in range(1, len(data)):
        #print(data[i])
        
        p = Piece()
        
        if data[i][1] != "":
            p.pos = int(data[i][1])
        
        p.ref   = data[i][2]
        p.quant = int(data[i][4])
        p.mat   = data[i][5]
        
        # Check if has dimensions
        if data[i][8] == "":
            out.append(p)
            continue
        
        isCil  = False
        isRect = False
        if data[i][8][0] == 'Ø':
            isCil = True
        elif data[i][8][0].isdigit():
            isRect = True
        else:
            out.append(p)
            continue
        
        if isCil:
            dims = [float(a.strip()) for a in data[i][8][1:].split("x")]
            p.dimCil = True
            p.dimd = dims[0]
            p.dimz = dims[1]
        elif isRect:
            dims = [float(a.strip()) for a in data[i][8].split("x")]
            if len(dims) != 3:
                out.append(p)
                continue
            p.dimRect = True
            p.dimx = dims[0]
            p.dimy = dims[1]
            p.dimz = dims[2]
        elif isManual:
            p.isManual = True
            p.dimStr = data[i][8]
        
        out.append(p)
    
    return out


def csv_sinex2canonical(data):
    # Ignore first line
    
    out = []
    
    for i in range(1, len(data)):
        #print(data[i])
        p = Piece()
        if data[i][1] != "":
            p.pos = int(data[i][1])
        p.ref = data[i][2]
        p.mat = data[i][3]
        p.quant = int(data[i][5])
        
        isRect = data[i][12] != "" and data[i][13] != "" and data[i][14] != ""
        isCil  = data[i][14] != "" and data[i][15] != ""
        if isRect:
            p.dimRect = True
            p.dimx    = float(data[i][12])
            p.dimy    = float(data[i][13])
            p.dimz    = float(data[i][14])
        elif isCil:
            p.dimCil = True
            p.dimz = float(data[i][14])
            p.dimd = float(data[i][15])
        
        out.append(p)
    
    return out


def piece_get_diff(piece0, piece1):
    diff = {}
    diff['ref']     = piece0.ref     == piece1.ref
    diff['pos']     = piece0.pos     == piece1.pos
    diff['mat']     = piece0.mat     == piece1.mat
    diff['quant']   = piece0.quant   == piece1.quant
    diff['dimRect'] = piece0.dimRect == piece1.dimRect
    diff['dimCil']  = piece0.dimCil  == piece1.dimCil
    diff['dimx']    = piece0.dimx    == piece1.dimx
    diff['dimy']    = piece0.dimy    == piece1.dimy
    diff['dimz']    = piece0.dimz    == piece1.dimz
    diff['dimd']    = piece0.dimd    == piece1.dimd
    return diff


def piece_print_diff(diff):
    print('(', end="")
    for d in diff:
        if not diff[d]:
            print(d + ', ', end="")
    print(')')


# dumb mechanism
#TODO
def piece_print_formated(piece, width = {}, color = {}):
    fields = ['pos', 'ref', 'mat', 'quant', 'dimRect', 'dimCil', 'dimx',\
              'dimy', 'dimz', 'dimd']
    print(piece.pos, end="")
    print(piece.ref, end="")

#TODO
def piece_showdiff(piece0, piece1):
    return


#FIXME: Tentar libertar-me da referência como identificação.
# Em caso de peças com referências duplicadas, a referência perde a utilidade
# como identificação.
def csv_compare(pieces0, pieces1):
    dic0 = {}
    for p in pieces0:
        if p.ref not in dic0:
            dic0[p.ref] = []
        dic0[p.ref].append(p)

    dic1 = {}
    for p in pieces1:
        if p.ref not in dic1:
            dic1[p.ref] = []
        dic1[p.ref].append(p)
    
    print("DUPLICADOS NA PRIMEIRA LISTA")
    for ref in dic0:
        if len(dic0[ref]) > 1:
            print(ref, ":", len(dic0[ref]), "cópias")
    print()
    print("DUPLICADOS NA SEGUNDA LISTA")
    for ref in dic1:
        if len(dic1[ref]) > 1:
            print(ref, ":", len(dic1[ref]), "cópias")
    
    
    # Comparisons
    set0 = set(dic0.keys())
    set1 = set(dic1.keys())
    only0 = list(set0 - set1)
    only0.sort()
    only1 = list(set1 - set0)
    only1.sort()
    comm  = list(set0.intersection(set1))
    comm.sort()
    
    print()
    print("PEÇAS APENAS NA PRIMEIRA LISTA")
    for p in only0:
        print(p)
    
    print()
    print("PEÇAS APENAS NA SEGUNDA LISTA")
    for p in only1:
        print(p)
    
    print()
    print("PEÇAS COM PROPRIEDADES DIFERENTES")
    for ref in comm:
        #HACK: Talvez possa comparar peças entre elas
        if len(dic0[ref]) > 1 or len(dic1[ref]) > 1:
            continue
        p0 = dic0[ref][0]
        p1 = dic1[ref][0]
        if p0 != p1:
            diff = piece_get_diff(p0,p1)
            piece_print_diff(diff)
            p0.print()
            p1.print()
            print()



model_fn = sys.argv[1]
sinex_fn = sys.argv[2]

model_data = csv_read_data(model_fn, encoding="cp1252")
sinex_data = csv_read_data(sinex_fn, encoding="utf_8_sig", delimiter=";")

model_pieces = csv_model2canonical(model_data)
sinex_pieces = csv_sinex2canonical(sinex_data)

csv_compare(model_pieces, sinex_pieces)