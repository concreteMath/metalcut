import ctypes
import time

#TODO Faltam estatísticas dos tempos obtidos

def get_click_times():
    mouseDown = False
    t0 = -1
    while True:
        s = ctypes.windll.user32.GetKeyState(1)
        t = time.time()
        if mouseDown and (s == 0 or s==1):
            mouseDown = False
            print("%.4f"%(t - t0))
        if not mouseDown and (s != 0 and s!=1):
            mouseDown = True
            t0 = t
        time.sleep(0.001)

get_click_times()