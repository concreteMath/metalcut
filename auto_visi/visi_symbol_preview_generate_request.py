import sys
import os
import time

cmd_dir = "\\\\pjfserver\\e\\6.PARTILHA\\diamantino\\edw_cmd"

def timestamp():
	t = time.time()
	return "%.3f"%t

def make_cmd(symbol_path, image_path):
	cmd_path = cmd_dir + "\\_cmd_" + timestamp()
	cmd_text = "visi_symbol_preview\n%s\n%s\n"%(symbol_path, image_path)
	fp = open(cmd_path, "w")
	fp.write(cmd_text)
	fp.close()


symbol_path = sys.argv[1]
image_fn    = os.path.basename(symbol_path)[:-4] + ".png"
image_dir   = os.path.dirname(symbol_path)
image_path  = image_dir + "\\" + image_fn

make_cmd(symbol_path, image_path)
