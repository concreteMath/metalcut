import os
import subprocess as sp
import time
import PIL
import pyautogui

# TODO: o truque é deixar o sistema em standby para facilmente executar os comandos para gerar o png

watchDir = "\\\\pjfserver\\e\\6.PARTILHA\\diamantino\\edw_cmd"
visiExe  = "C:\\VISI20\\bin64\\Visi.exe"

def getTasks():
	p = sp.run(["tasklist"], stdout=sp.PIPE)
	outStr = p.stdout.decode('latin1')
	tasks = outStr.split('\r\n')
	
	# Get Fields sizes
	fieldsLenStr = tasks[2].split(' ')
	fi0 = 0
	fi1 = 0
	fieldsRange = []
	for i, f in enumerate(fieldsLenStr):
		fi1 = fi0 + len(f)
		fieldsRange.append([fi0, fi1])
		fi0 = fi1 + 1
	
	# Build task list information
	tasks_out = []
	for i in range(3,len(tasks)-1):
		tasks_out.append([])
		for j in range(5):
			fi0 = fieldsRange[j][0]
			fi1 = fieldsRange[j][1]
			tasks_out[-1].append(tasks[i][fi0:fi1].strip())
		#print(tasks_out[-1])
	return tasks_out


def taskIsRunning(taskName):
	tasks = getTasks()
	for t in tasks:
		if t[0] == taskName:
			return True
	return False


def visi_start():
	if not taskIsRunning("Visi.exe"):
		print("VISI not running. Starting")
		sp.Popen([visiExe], stderr=sp.PIPE, stdout=sp.PIPE)
		time.sleep(20)
	while not taskIsRunning("Visi.exe"):
		print("waiting to open VISI")
		time.sleep(2)
	return


def visiMakePreview(symbol_fn, image_fn):
	# Change to visi
	# HACK: super+D; super+4 (4 é o número do visi)
	
	pyautogui.keyDown('win')
	pyautogui.press('d')
	pyautogui.keyUp('win')
	time.sleep(0.5)
	
	pyautogui.keyDown('win')
	pyautogui.press('4')
	pyautogui.keyUp('win')
	time.sleep(1.0)
	
	# Maximize visi
	#pyautogui.keyDown('alt')
	#pyautogui.press('space')
	#pyautogui.keyUp('alt')
	
	# Open Layout file
	pyautogui.keyDown('ctrlleft')
	pyautogui.press('n')
	pyautogui.keyUp('ctrlleft')
	time.sleep(0.1)
	pyautogui.press('enter')
	time.sleep(5.0)
	
	# Change to layout
	pyautogui.keyDown('ctrlleft')
	pyautogui.press('h')
	pyautogui.keyUp('ctrlleft')
	
	# Insert symbol
	time.sleep(0.5)
	pyautogui.keyDown('ctrlleft')
	pyautogui.press('x')
	pyautogui.keyUp('ctrlleft')
	# paste symbol address
	time.sleep(0.5)
	pyautogui.write(symbol_fn)
	pyautogui.press('enter')
	time.sleep(0.2)
	
	pyautogui.moveTo(960,540)
	pyautogui.click()
	time.sleep(0.2)
	pyautogui.press('esc')
	
	
	#pyautogui.keyDown('ctrlleft')
	#pyautogui.press('x')
	#pyautogui.keyUp('ctrlleft')
	
	# Zoom Extend
	pyautogui.keyDown('altleft')
	pyautogui.press('space')
	pyautogui.keyUp('altleft')
	pyautogui.press('left')
	pyautogui.press('left')
	pyautogui.press('up')
	pyautogui.press('up')
	pyautogui.press('right')
	pyautogui.press('down')
	pyautogui.press('enter')
	time.sleep(0.2)
	
	# Enter Save As
	pyautogui.keyDown('altleft')
	pyautogui.press('space')
	pyautogui.keyUp('altleft')
	pyautogui.press('right')
	pyautogui.press('right')
	pyautogui.press('down')
	pyautogui.press('down')
	pyautogui.press('down')
	pyautogui.press('enter')
	time.sleep(0.2)
	
	# Save PNG
	# FIXME: verificar se o png já existe
	if os.path.isfile(image_fn):
		os.remove(image_fn)
	
	pyautogui.write(image_fn)
	pyautogui.press('tab')
	pyautogui.press('p')
	pyautogui.press('p')
	pyautogui.press('enter')

def isCmdFile(path):
	basename = os.path.basename(path)
	return basename[:4] == "_cmd"


#TODO o comando deveria vir em format json ou num formatado standard qualquer
def cmdFile_read(path):
	#fl = os.listdir(watchDir)
	#path = watchDir + "\\" + f
	fp = open(path, "r")
	args = fp.read().strip().split('\n')
	fp.close()
	return args


def waitForCmd():
	watch_dt = 2.0
	while True:
		print("Search cmd")
		files = os.listdir(watchDir)
		files.sort()
		for f in files:
			path = watchDir + "\\" + f
			if isCmdFile(path):
				cmd_args = cmdFile_read(path)
				return path, cmd_args
		time.sleep(watch_dt)

def cmd_delete(path):
	os.remove(path)

#print(taskIsRunning('Visi.exe'))
visi_start()
print("Visi is started")

# MAIN LOOP
while True:
	# Esperar por um comando
	path, cmd_args = waitForCmd()
	print(path, cmd_args)
	cmd = cmd_args[0]
	if cmd == "visi_symbol_preview":
		symbol_fn = cmd_args[1]
		image_fn  = cmd_args[2]
		visiMakePreview(symbol_fn, image_fn)
	cmd_delete(path)

#symbol_fn = "\\\\pjfserver\\E\\2.MEGADIES\\3.DESENVOLVIMENTO\\Mascaras2D\\cavilha.tol_8.sym"
#image_fn  = "\\\\pjfserver\\E\\2.MEGADIES\\3.DESENVOLVIMENTO\\Mascaras2D\\cavilha.tol_8.png"
#doPreview(symbol_fn, image_fn)
