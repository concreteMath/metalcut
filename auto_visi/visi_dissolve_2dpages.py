import pyautogui, sys
import time
import ctypes

# NOTA
# Este script deve ser executado num terminal em modo de administrador, de modo
# a que o VISI detecte os clicks.

#TODO detectar a posição da janela do visi?


# https://stackoverflow.com/questions/165495/detecting-mouse-clicks-in-windows-using-python
# https://stackoverflow.com/a/46596592
def get_click_pos():
    while True:
        s = ctypes.windll.user32.GetKeyState(1)
        if s != 0 and s!=1:
            break
        # Na versão original estava 0.001s, mas creio que 0.01s seja suficiente.
        # Depois de alguns testes, o clique mais rápido que consegui foi 0.007s
        time.sleep(0.01)
    return pyautogui.position()


print(" #######################")
print(" # VISI MASS DISSOLVER #")
print(" #######################")
print()
#print("layer blank position")
print("PREPARAÇÃO")

print("- Mova a janela deste script para fora do visi.")
print("- Maximize o VISI")
print("- Seleccione a primeira \"Plotview Page\" a dissolver")

print("- Carregue ENTER quanto estiver pronto")
input("\n")

print("- Clique entre a árvore de folhas e o limite do painel do \"Plotview Manager\"")

#plot_blank_pos = pyautogui.position()

plot_blank_pos = get_click_pos()
time.sleep(0.2)

#print(get_click_pos())
#exit(1)

max_drawings = 500

for i in range(max_drawings):
    
    # Select Drawing
    pyautogui.moveTo(100, 950)
    pyautogui.click()
    
    # Enter dissolve
    pyautogui.keyDown('ctrlleft')
    pyautogui.press('g')
    pyautogui.keyUp('ctrlleft')
    
    # Dissolve - select all
    time.sleep(0.1)
    pyautogui.moveTo(40, 670)
    pyautogui.click()
    time.sleep(1)
    
    # Layer
    pyautogui.moveTo(plot_blank_pos)
    pyautogui.click()
    time.sleep(0.1)
    pyautogui.keyDown("down")
    time.sleep(1)