# Geral
[ ] Introduzir rotinas para lidar com os erros que possam surgir


# Windows
[ ] Eliminar as animações do windows
	- Quero respostas instantâneas
[ ] Desligar o bloqueio automática da conta
	- O desktop tem que estar continuamente exposto
[ ] Encontrar uma forma de lidar directamente com janelas em vez daquele hack "super+D;super+4"
[ ] Descobrir como sacar as medidas e posições de janelas


# Server
[ ] Criar um sistema server-client
[ ] Comunicar por um RPC
[ ] Não deixar que os comandos possam manipular coisas do computador
	- Não mexer em ficheiros
	- Não executar comandos arbitrários
[ ] Durante a execução de um comando, ir dando feedback ao user


# VISI
[ ] Criar rotinas para as acções do visi
	- ex: visi_saveAs(fn)
	- ex: visi_exportCSV()
[ ] Minimizar o tempo gasto por rotina
	- Reduzir as esperas sem comprometer o resultado final
	[ ] Analisar o limite de resposta de cada acção
		- ex: movimentos nos menus
[ ] Talvez substituir os tempos de espera da execução das rotinas por confirmações de fim de rotina
	- Essas confirmações podem ser visuais, como uma mensagem ou uma barra de progresso finalizada
[ ] Talvez criar atalhos de teclado para as operações mais comuns
[ ] Criar rotinas de leitura do tipo de letra usado no VISI
	- Creio que seja "MS Sans-Serif"


# Trabalhos exemplo
[ ] Automatizar a criação de placas de choque
