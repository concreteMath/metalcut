#!/usr/bin/python3

from lib import *
from sys import stdout, argv

pieces_dir = argv[1]

boardH = int(argv[2])
boardW = int(argv[3])


pieces = []
for i in range(1,8):
	img = pbm_read(pieces_dir + "/p%i.pbm"%i)
	pieces.append(img)

progressive_solve(pieces, boardH, boardW)
