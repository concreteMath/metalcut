#!/usr/bin/python3

from lib import *
from sys import stdout, argv

pieces_dir = argv[1]

boardH = int(argv[2])
boardW = int(argv[3])


pieces = []
files = os.listdir(pieces_dir)
files.sort()
for fn in files:
	if fn[-4:] != ".pbm":
		continue
	img = pbm_read(pieces_dir + "/" + fn)
	pieces.append(img)

board, tries = brute_solve(pieces, boardH, boardW)

H, W = board.shape

if 1:
	for i in range(0,H,2):
		for j in range(W):
			c = 'O' if board[i,j] else '.'
			stdout.write(c)
		stdout.write('\n')
	print("Tries:", tries)

print(tries)
