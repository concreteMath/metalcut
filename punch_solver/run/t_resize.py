#!/usr/bin/python3

from sys import argv
from lib import *

img_fn = argv[1]
scale  = int(argv[2])

img = pbm_read(img_fn)
img2 = img_powerReduce(img, scale)
print_array2d(img2)
