import os
from sys import stdout
import numpy as np

def pbm_read(fn):
	#FIXME Este return tá mal. Uma imagem vazia retorna uma matriz vazia
	if not os.path.isfile(fn):
		return np.zeros((0,0), dtype = np.bool)
	
	with open(fn, "rb") as fp:
		data = fp.read()
	
	i0 = 0
	i  = -1 
	for j in range(2):
		i0 = i
		for i in range(i0+1, len(data)):
			if data[i] == 0x0a:
				break
	W, H = [int(n) for n in data[i0+1:i].split()]
	data = data[i+1:]

	img = np.zeros((H,W), dtype= np.bool)

	W1 = (len(data)*8)//H

	for i in range(H):
		for j in range(W):
			B = (i*W1 + j)//8			
			mask = 0x80>>(j%8)
			img[i,j] = 0 if data[B]&mask else 1

	return img


def print_array2d(a):
	H, W = a.shape
	for i in range(0,H,2):
		for j in range(W):
			c = 'O' if a[i,j] else '.'
			stdout.write(c)
		stdout.write('\n')


# Given a certain area of the original image to resize to a single pixel,
# if all pixels in that certain area are 1, then the pixel becomes 1, otherwise
# becomes 0.
def img_powerReduce(img, power):
	unit = 2**power
	H,W  = img.shape
	uH   = H//unit
	uW   = W//unit
	rH   = H%unit
	rW   = W%unit

	finalH = uH + (rH != 0)*1
	finalW = uW + (rW != 0)*1

	imgOut = np.zeros((finalH, finalW), bool)

	for i in range(uH):
		for j in range(uW):
			imgOut[i,j ] = np.all(img[i*unit:(i+1)*unit, j*unit:(j+1)*unit])
		if (rW):
			imgOut[i,uW] = np.all(img[i*unit:(i+1)*unit, uW*unit:])
	
	if (rH):
		for j in range(uW):
			imgOut[uH,j ] = np.all(img[uH*unit:, j*unit:(j+1)*unit])
		if (rW):
			imgOut[uH,uW] = np.all(img[uH*unit:, uW*unit:])
	
	return imgOut


def brute_solve(pieces, H, W):

	# Preparação das peças rodadas
	pieces_rot = []
	for j in range(len(pieces)):
		pieces_rot.append([])
		for k in range(4):
			pieces_rot[j].append(np.rot90(pieces[j], k))

	for i in range(10000):
		#print("try:", i)
		board = np.zeros((H,W), bool)
		flag_done = True
		# Shuffle pieces
		pieces_index = list(range(len(pieces)))
		np.random.shuffle(pieces_index)
		for j in range(len(pieces)):
			flag_piece_done = False
			pj = pieces_index[j]
			for k in range(200):
				# Rotate image
				r = np.random.randint(4)
				m = pieces_rot[pj][r]

				imgH, imgW = m.shape

				iRandRange = H - imgH + 1
				jRandRange = W - imgW + 1
				if iRandRange <= 0 or jRandRange <= 0:
					flag_piece_done = False
					break

				di = np.random.randint(H - imgH + 1)
				dj = np.random.randint(W - imgW + 1)
				
				if not np.any(m & board[di:di+imgH, dj:dj+imgW]):
					board[di:di+imgH, dj:dj+imgW] |= m
					flag_piece_done = True
					break

			if flag_piece_done == False:
				flag_done = False
				break

		if flag_done:
			break
	
	return board, i


def solve_step(pieces, pos, H, W):
	print((H,W))
	pos_out = [[0,0] for i in range(len(pieces))]
	for a in range(10000):
		board     = np.zeros((H,W), bool)
		flag_done = True
		# Shuffle pieces
		pieces_index = list(range(len(pieces)))
		np.random.shuffle(pieces_index)
		for b in range(len(pieces)):
			flag_piece_done = False
			p = pieces_index[b]
			for k in range(200):
				m = pieces[p]
				imgH, imgW = m.shape
				print(imgH, imgW, pos[p])
				i0 = pos[p][0] + np.random.randint(2)
				j0 = pos[p][1] + np.random.randint(2)

				#FIXME isto é remendo
				i0 = H-1 if i0>=H else i0
				j0 = W-1 if j0>=W else j0

				#FIXME: preciso de verificar se a peça ultrapassa os limites
				# do tabuleiro
				
				if not np.any(m & board[i0:i0+imgH, j0:j0+imgW]):
					board[i0:i0+imgH, j0:j0+imgW] |= m
					flag_piece_done = True
					pos_out[p][0] = i0
					pos_out[p][1] = j0
					break
			if flag_piece_done == False:
				flag_done = False
				break
		if flag_done:
			break
	
	print_array2d(board)
	return pos_out

# Progressivelly findind optimal positions
def progressive_solve(pieces_in, H, W):
	n_pieces = len(pieces_in)
	pos = []
	for	i in range(n_pieces):
		pos.append([0,0])
	
	# Preparação das peças rodadas
	pieces_rot = []
	for i in range(n_pieces):
		r = np.random.randint(4)
		pieces_rot.append(np.rot90(pieces_in[i], r))
	
	#FIXME deve ser calculada em funçaõ de H e W
	n_scales = 10
	for i in range(n_scales):
		# Rescale coordinates
		for j in range(n_pieces):
			pos[j][0] *= 2
			pos[j][1] *= 2
		# Rescale the board
		side = 2**(n_scales-1-i)
		H_scaled = (H-1)//side + 1
		W_scaled = (W-1)//side + 1

		# Rescale pieces
		pieces_scaled = []
		for piece in pieces_rot:
			pieces_scaled.append(img_powerReduce(piece, n_scales-1-i))

		# Solve this
		pos = solve_step(pieces_scaled, pos, H_scaled, W_scaled)
		print(pos)
	
	return pos
