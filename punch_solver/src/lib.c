#include "lib.h"

Array2d zeros(int32_t H, int32_t W) {
	Array2d a;
	a.ptr = malloc(H*sizeof(uint8_t*));
	for (int i=0; i<H; i++) {
		(a.ptr)[i] = calloc(W, sizeof(uint8_t));
	}
	a.dim0 = H;
	a.dim1 = W;
	return a;
}


int8_t Array2d_free(Array2d* a)
{
	for (int i=0; i<a->dim0; i++) {
		free((a->ptr)[i]);
	}
	free(a->ptr);
	a->dim0 = 0;
	a->dim1 = 0;
	return 0;
}

int8_t Array2d_fill(Array2d* a, uint8_t value) {
	for (int i=0; i<a->dim0; i++) {
		for (int j=0; j<a->dim1; j++) {
			(a->ptr)[i][j] = value;
		}
	}
	return 0;
}


Array2d Array2d_rot90(Array2d* a, int n) {
	int32_t H = a->dim0;
	int32_t W = a->dim1;
	uint8_t rot_angle = n%4;
	Array2d a1;

	//FIXME: deve haver uma forma mais limpa de escrever este código, tirando
	// partido das simetrias das operações.
	switch(rot_angle) {
	case 0:
		a1 = zeros(H, W);
		for(int i=0; i<H; i++) {
			memcpy((a1.ptr)[i], (a->ptr)[i], W*sizeof(uint8_t));
		}
		break;
	case 1:
		a1 = zeros(W, H);
		for(int i=0; i<H; i++) {
			for(int j=0; j<W; j++) {
				(a1.ptr)[W-1-j][i] = (a->ptr)[i][j];
			}
		}
		break;
	case 2:
		a1 = zeros(H, W);
		for(int i=0; i<H; i++) {
			for(int j=0; j<W; j++) {
				(a1.ptr)[i][j] = (a->ptr)[H-1-i][W-1-j];
			}
		}
		break;
	case 3:
		a1 = zeros(W, H);
		for(int i=0; i<H; i++) {
			for(int j=0; j<W; j++) {
				(a1.ptr)[j][H-1-i] = (a->ptr)[i][j];
			}
		}
		break;
	}
	return a1;
}


uint8_t Array2d_any_region(int32_t i0, int32_t j0, Array2d* a0, Array2d* a1)
{
	for (int i=0; i<a1->dim0; i++) {
		for (int j=0; j<a1->dim1; j++) {
			if ((a0->ptr)[i0+i][j0+j] & (a1->ptr)[i][j]) {
				return 1;
			}
		}
	}
	return 0;
}


int8_t Array2d_or_region(int32_t i0, int32_t j0, Array2d* a0, Array2d* a1)
{
	for (int i=0; i<a1->dim0; i++) {
		for (int j=0; j<a1->dim1; j++) {
			(a0->ptr)[i+i0][j+j0] |= (a1->ptr)[i][j];
		}
	}
	return 0;
}


int8_t Array2d_print(Array2d* a)
{
	for(int i=0; i<a->dim0; i += 2) {
		for(int j=0; j<a->dim1; j++) {
			printf("%c", (a->ptr)[i][j] ? 'O' : '.');
		}
		printf("\n");
	}
	return 0;
}


//TODO falta lidar com os erros.
Array2d pbm_read(char* fn)
{
	FILE* fp = fopen(fn, "rb");
	if (!fp) {
		fprintf(stderr, "File not found\n");
		return zeros(0,0);
	}

	int32_t H = 0;
	int32_t W = 0;

	while (1) {
		if (fgetc(fp) == 0x0a) {
			break;
		}
	}
	fscanf(fp, "%i %i\n", &W, &H);

	Array2d imgObj = zeros(H, W);
	uint8_t **img = imgObj.ptr;

	// Calculation of the number of bytes per row
	int32_t firstBytes  = W/8;
	int32_t lastBits    = W%8;

	uint8_t b = 0;
	int32_t j = 0;


	for (int i=0; i<H; i++) {
		j = 0;
		for (int k=0; k<firstBytes; k++) {
			b = fgetc(fp);
			for (int l=0; l<8; l++) {
				img[i][j] = (b & (0x80>>l)) == 0;
				j++;
			}
		}
		if (lastBits) {
			b = fgetc(fp);
			for (int l=0; l<lastBits; l++) {
				img[i][j] = (b & (0x80>>l)) == 0;
				j++;
			}
		}
	}

	fclose(fp);
	
	imgObj.dim0 = H;
	imgObj.dim1 = W;
	return imgObj;
}


Array1d Array1d_zeros(int32_t n) {
	Array1d a;
	a.ptr = calloc(n, sizeof(uint8_t));
	a.dim0 = n;
	return a;
}

int8_t Array1d_free(Array1d* a) {
	free(a->ptr);
	return 0;
}

int8_t Array1d_fillShuff(Array1d* a) {
	int32_t  n = a->dim0;
	int32_t* t = malloc(n * sizeof(int32_t));
	for (int i=0; i<n; i++) {
		t[i] = i;
	}
	int32_t r = 0;
	int32_t v = 0;
	for (int i=0; i<n-1; i++) {
		//FIXME tirar o bias deste rand
		r = rand()%(n-i);
		v = t[r];
		//printf("v:%i\n", v);
		if (r != n-1-i) {
			t[r] = t[n-1-i];
		}
		(a->ptr)[i] = v;
	}
	(a->ptr)[n-1] = t[0];
	free(t);
	return 0;
}

uint32_t seed_from_urand()
{
	FILE* fp = fopen("/dev/urandom", "rb");
	uint32_t s = 0;
	for (int i=0; i<4; i++) {
		s |= fgetc(fp)<<(8*i);
	}
	fclose(fp);
	return s;
}


Array2d brute_solve(int32_t n_pieces, Array2d* pieces, int32_t H, int32_t W,
                    int32_t* iterations)
{
	// Preparação das peças rodadas
	Array2d** pieces_rot = malloc(n_pieces * sizeof(Array2d*));
	for (int i=0; i<n_pieces; i++) {
		pieces_rot[i] = malloc(4 * sizeof(Array2d));
		for(int j=0; j<4; j++) {
			pieces_rot[i][j] = Array2d_rot90(&(pieces[i]), j);
		};
	}

	Array2d board           = zeros(H, W);
	Array2d m;
	Array1d pieces_index    = Array1d_zeros(n_pieces);
	uint8_t flag_done       = 0;
	uint8_t flag_piece_done = 0;
	int32_t pj = 0;
	int32_t r  = 0;
	int32_t imgH, imgW, di, dj;
	//int32_t 

	int i = 0;
	for (i=0; i<100000; i++) {
		Array2d_fill(&board, 0);
		Array1d_fillShuff(&pieces_index);
		flag_done = 1;
		for (int j=0; j<n_pieces; j++) {
			flag_piece_done = 0;
			pj = pieces_index.ptr[j];
			for (int k=0; k<200; k++) {
				r = rand()%4;
				m = pieces_rot[pj][r];

				imgH = m.dim0;
				imgW = m.dim1;

				if (H - imgH < 0 || W - imgW < 0) {
					flag_piece_done = 0;
					break;
				}

				//FIXME Fazer uma função de rand decente
				di = rand()%(H - imgH + 1);
				dj = rand()%(W - imgW + 1);


				if (!Array2d_any_region(di, dj, &board, &m)) {
					Array2d_or_region(di, dj, &board, &m);
					flag_piece_done = 1;
					break;
				}
			}

			if (!flag_piece_done) {
				flag_done = 0;
				break;
			}
		}
		if (flag_done) {
			break;
		}
	}


	Array1d_free(&pieces_index);
	
	// Free pieces
	for (int i=0; i<n_pieces; i++) {
		for (int j=0; j<4; j++) {
			Array2d_free(&(pieces_rot[i][j]));
		}
		free(pieces_rot[i]);
	}
	free(pieces_rot);

	*iterations = i;

	return board;
}
