#include <stdio.h>
#include <time.h>
#include "lib.h"

uint8_t test0()
{
	Array2d m1 = pbm_read("tests/test0/pecas/p2.pbm");
	Array2d_print(&m1);
	Array2d m  = Array2d_rot90(&m1, 3);
	Array2d_print(&m);

	Array2d_free(&m);
	Array2d_free(&m1);

	return 0;
}


uint8_t test1(char* punch_dir, int n_pieces, int H, int W)
{
	srand(seed_from_urand());

	int32_t n = n_pieces;

	Array2d* pieces = malloc(n*sizeof(Array2d));

	char fn[256] = {0};

	for (int i=0; i<n; i++) {
		sprintf(fn, "%s/p%i.pbm", punch_dir, i);
		printf("%s\n", fn);
		pieces[i] = pbm_read(fn);
		//Array2d_print(&(pieces[i]));
	}
	
	int32_t iterations = 0;
	Array2d board = brute_solve(n, pieces, H, W, &iterations);

	Array2d_print(&board);
	Array2d_free(&board);

	for (int i=0; i<n; i++) {
		Array2d_free(pieces + i);
	}
	free(pieces);

	printf("%i\n", iterations);

	return 0;
}


int main(int argc, char* argv[])
{
	char* punch_dir = argv[1];
	int n_pieces = atoi(argv[2]);

	int H = atoi(argv[3]);
	int W = atoi(argv[4]);

	test1(punch_dir, n_pieces, H, W);

	return 0;
}
