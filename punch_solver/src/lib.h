#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

typedef struct Array1d
{
	int32_t dim0;
	uint8_t *ptr;
} Array1d;

Array1d Array1d_zeros(int32_t n);

int8_t Array1d_free(Array1d* a);

int8_t Array1d_fillShuff(Array1d* a);


typedef struct Array2d
{
	int32_t dim0;
	int32_t dim1;
	uint8_t **ptr;
} Array2d;

Array2d zeros(int32_t H, int32_t W);

int8_t Array2d_free(Array2d* a);

int8_t Array2d_fill(Array2d* a, uint8_t value);

Array2d Array2d_rot90(Array2d* a, int32_t n);

uint8_t Array2d_any_region(int32_t i0, int32_t j0, Array2d* a0, Array2d* a1);

int8_t Array2d_or_region(int32_t i0, int32_t j0, Array2d* a0, Array2d* a1);

int8_t Array2d_print(Array2d* a);

Array2d pbm_read(char* fn);

uint32_t seed_from_urand();

Array2d brute_solve(int32_t n_pieces, Array2d* pieces, int32_t H, int32_t W,
                    int32_t* iterations);

