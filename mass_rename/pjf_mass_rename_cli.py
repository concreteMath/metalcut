import sys
import lib_pjf_mass_rename as mr

dirname = sys.argv[1]
csv_fn  = sys.argv[2]

mr.drawingDirFiles_rename_with_csv(dirname, csv_fn)