import os
#import sys
import re
import csv


def file_is_drawing(fn):
	extension = fn[-4:].lower()
	return extension == '.dxf' or extension == '.dwg' or extension == '.stp'


def file_is_csv(fn):
	extension = fn[-4:].lower()
	return extension == '.csv'


def drawing_get_ref(filepath):
	basename = os.path.basename(filepath)
	m = re.search(r'([A-Z][0-9]+)\$', basename)
	if m == None:
		return None
	else:
		ref = m.group(1)
		return ref


def csv_read_data(csv_filename):
	if not os.path.isfile(csv_filename):
		print(csv_filename, "not found")
		return None
	data = []
	csv_fp = open(csv_filename, "r")
	csv_data = csv.reader(csv_fp)
	
	return [row for row in csv_data]


#TODO A função também deverá reportar os casos de não bijecção
def csv_to_dic(csv_path):
	out_dic = {}
	csv_data = csv_read_data(csv_path)
	
	# Error handling
	if csv_data == None:
		return None
	
	ref2pos_raw = {}
	pos2ref_raw = {}
	
	# Build raw correspondence between |ref| and |pos| (many to one or one to many)
	for row in csv_data:
		ref = row[2]
		pos = row[1]
		if ref not in ref2pos_raw:
			ref2pos_raw[ref] = []
		if pos not in pos2ref:
			pos2ref_raw[pos] = []
		ref2pos_raw[ref].append(pos)
		pos2ref_raw[pos].append(ref)
	
	# Blacklist all |ref| or |pos| related to a non-bijective relation
	ref_blackList = set()
	pos_blackList = set()
	
	for ref in ref2pos_raw:
		if len(ref2pos_raw[ref]) > 1:
			ref_blackList.add(ref)
			pos_blackList.union(set(ref2pos_raw[ref]))
	
	for pos in pos2ref_raw:
		if len(pos2ref_raw[pos]) > 1:
			pos_blackList.add(pos)
			ref_blackList.union(set(pos2ref_raw[pos]))
	
	# Build the bijective correspondence
	ref2pos = {}
	for row in csv_data:
		ref = row[2]
		pos = row[1]
		if ref in ref_blackList or pos in pos_blackList:
			continue
		else:
			ref2pos[ref] = pos
	
	return ref2pos


def get_filepaths(dirname):
	if not os.path.isdir(dirname):
		return None
	
	all_files = os.listdir(dirname)
	all_files.sort()

	filepaths = [dirname + "//" + fn for fn in all_files]
	return filepaths
	

# Não assumir que os ficheiros estão todos na mesma pasta
# O que interessa é que não haja 2 ou mais renames iguais
# Assumo que o csv_dic é bijectivo
# Nota: para paths iguais, tenho renames iguais, portanto, só tenho de preocupar com paths diferentes
# darem renames iguais
def get_valid_renames(filepaths, csv_dic):
		
	if filenames == None or csv_dic == None:
		return None
	
	rename_counter = {}
	renames_maybe = []

	for fp in filepaths:

		# The file must be a drawing
		if not file_is_drawing(fp):
			continue
		ref = drawing_get_ref(fp)
		# The reference must exist
		if ref == None:
			continue
		# The reference must be in the dictionary
		if ref not in csv_dic:
			continue

		# Build new path
		pos     = csv_dic[ref]
		#TODO a extracção da extensão tá mal feita
		# A extensão é a string a seguir ao último ponto
		ext     = fp[-4:]
		dirname = os.path.dirname(path_old)
		fp_new  = dirname + "\\" + pos + "." + ext

		renames_maybe.append((fp, fp_new))

		# Count the number of renames to the same path
		if fp_new not in rename_counter:
			rename_counter[fp_new] = 0
		rename_counter[fp_new] += 1
	

	# Filter the valid renames
	# If multiple files map to the same new path, they must be cancelled
	renames_ok = []
	for r in renames_maybe:
		if rename_counter[r[1]] != 1:
			continue
		renames_ok.append(r)
	
	return renames_ok


def mass_rename(rename_list):
	for r in rename_list:
		if not os.path.isfile(r[0]):
			continue
		else:
			os.rename(r[0], r[1])


# Esta função deve fazer o rename e retornar uma lista dos erros que impediram
# o rename de certos ficheiros.
def drawingDirFiles_rename_with_csv(dirname, csv_path):
	# This dictionary garantees bijection ref<->pos
	csv_dic = csv_to_dic(csv_path)
	if csv_dic == None:
		return None
	
	# Raw file list
	filepaths = get_filepaths(dirname)

	# Filter the non-valid renames
	# Conditions:
	# 1) must be a drawing
	# 2) must not have the same reference as other file
	# 3) the file reference must be in the dictionary
	rename_list = get_valid_renames(filepaths, csv_dic)
	
	# Mass rename
	mass_rename(rename_list)
	
	# Falta a colecção de erros
	return []
