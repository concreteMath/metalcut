import os
import re
import csv

#TODO repensar os sistema dos erros

global errors
errors = []

#TODO: Falta produzir output de erros
def make_bijective_mapping(links):
	if links == None:
		errors.append("No links found")
		return None
	
	# Unique set items
	A = set()
	B = set()
	# Unique relations
	map_uniq = set()
	
	for m in links:
		if len(m) < 2:
			continue
		if m[0] == None or m[1] == None:
			continue
		
		map_uniq.add((m[0], m[1]))
		A.add(m[0])
		B.add(m[1])
	
	# Count items
	A_count = dict(zip(A, [0]*len(A)))
	B_count = dict(zip(B, [0]*len(B)))
	for m in map_uniq:
		A_count[m[0]] += 1
		B_count[m[1]] += 1
	
	# Filter non bijective relations
	map_out = []
	for m in map_uniq:
		if A_count[m[0]] != 1 or B_count[m[1]] != 1:
			continue
		map_out.append(m)
	
	return map_out


def file_is_drawing(fn):
	extension = fn[-3:].lower()
	return extension == 'dxf' or extension == 'dwg' or extension == 'stp'


def file_is_csv(fn):
	extension = fn[-3:].lower()
	return extension == 'csv'


def drawing_get_ref(filepath):
	basename = os.path.basename(filepath)
	#TODO melhorar a procura de referências para os desenhos.
	#EX: A9206-b será admissível?
	m = re.search(r'([A-Z][0-9]+)\$', basename)
	if m == None:
		return None
	else:
		ref = m.group(1)
		return ref


def csv_read_data(csv_filename):
	if not os.path.isfile(csv_filename):
		print(csv_filename, "not found")
		errors.append("CSV \"%s\" not found"%csv_filename)
		return None
	data = []
	csv_fp = open(csv_filename, "r")
	csv_data = csv.reader(csv_fp)
	
	return [row for row in csv_data]


#TODO A função também deverá reportar os casos de não bijecção
def csv_to_dic(csv_path):
	csv_data = csv_read_data(csv_path)
	# Error handling
	if csv_data == None:
		return None
	
	# Gather links, using the type (ref,pos)
	links = [(row[2],row[1]) for row in csv_data]
	
	# Build the bijective correspondence
	mapping = make_bijective_mapping(links)
	ref2pos = dict(mapping)
	
	return ref2pos


def get_filepaths(dirname):
	if not os.path.isdir(dirname):
		errors.append("%s not found"%dirname)
		return None
	
	all_files = os.listdir(dirname)
	all_files.sort()
	
	filepaths = [dirname + "\\" + fn for fn in all_files]
	return filepaths


# O que interessa é que não haja 2 ou mais renames iguais
# Assumo que o csv_dic é bijectivo

def get_valid_renames(filepaths, csv_dic):
	
	if filepaths == None or csv_dic == None:
		return None
	
	# O uso de sets eliminam renames iguais, evitanto o rename de um ficheiro
	# ao qual já foi feito rename
	renames_maybe = set()
	
	for fp in filepaths:
		
		# The file must be a drawing
		if not file_is_drawing(fp):
			continue
		ref = drawing_get_ref(fp)
		# The reference must exist
		if ref == None:
			continue
		# The reference must be in the dictionary
		if ref not in csv_dic:
			continue
		pos = csv_dic[ref]
		# The position must exist
		if pos == None:
			continue
		
		# Build new name
		fn  = os.path.basename(fp)
		fn_parts = fn.split('.')
		if len(fn_parts) == 1:
			ext = ''
		else:
			ext = fn_parts[-1]
		
		dirname = os.path.dirname(fp)
		fp_new  = dirname + "\\" + pos + "." + ext
		
		renames_maybe.add((fp, fp_new))
	
	# Get bijective mapping
	renames_ok = make_bijective_mapping(renames_maybe)
	
	return renames_ok


def mass_rename(rename_list):
	if rename_list == None:
		return
	for r in rename_list:
		if r[0] == None or r[1] == None:
			continue
		if not os.path.isfile(r[0]):
			continue
		os.rename(r[0], r[1])


# Esta função deve fazer o rename e retornar uma lista dos erros que impediram
# o rename de certos ficheiros.
def drawingDirFiles_rename_with_csv(dirname, csv_path):
	global errors
	errors = []
	
	# This dictionary garantees bijection ref<->pos
	csv_dic = csv_to_dic(csv_path)
	if csv_dic == None:
		return errors
	
	# Raw file list
	filepaths = get_filepaths(dirname)

	# Filter the non-valid renames
	# Conditions:
	# 1) must be a drawing
	# 2) must not have the same reference as other file
	# 3) the file reference must be in the dictionary
	rename_list = get_valid_renames(filepaths, csv_dic)
	
	#for r in rename_list:
	#	print(r)
	
	# Mass rename
	mass_rename(rename_list)
	
	errors.append("Done")
	
	return errors