from tkinter import filedialog
import tkinter as tk
import os
import time
import lib_pjf_mass_rename as mr

#TODO meter output a dizer que o processo acabou
#TODO meter output que coisas podem ter falhado

#TODO se clickar no browse e fizer cancelar, a path que está no input não deve mudar
#TODO se o input já tiver uma path, fazer browse nessa pasta

def clicked_dir():
	global last_dir
	current_dir = txt_dir.get()
	# Se nenhum directório estiver especificado, usa-se o último directório
	# global
	if current_dir == "":
		current_dir = last_dir
	dirname = filedialog.askdirectory(initialdir = current_dir, title = "Select file")
	# Se se cancelar a procura de directório, evita-se qualquer tipo de mudança
	# de estado
	if dirname == "":
		return
	dirname = dirname.replace("/", "\\")
	txt_dir.delete(0, 'end')
	txt_dir.insert(0, dirname)
	last_dir = dirname

def clicked_csv():
	global last_dir
	current_dir = os.path.dirname(txt_csv.get())
	# Se nenhum directório estiver especificado, usa-se o último directório
	# global
	if current_dir == "":
		current_dir = last_dir
	filename = filedialog.askopenfilename(initialdir = current_dir,\
	           title = "Select file", filetypes = (("csv files","*.csv"),\
	           ("all files","*.*")))
	# Se se cancelar a procura de ficheiro, evita-se qualquer tipo de mudança
	# de estado
	if filename == "":
		return
	filename = filename.replace("/", "\\")
	txt_csv.delete(0, 'end')
	txt_csv.insert(0, filename)
	last_dir = os.path.dirname(filename)

def output_errors(errors):
	t = "%.3f"%time.time()
	for err in errors:
		txt_out.insert("end", t + " " + str(err) + "\n")

def run():
	dirname  = txt_dir.get()
	csv_path = txt_csv.get()
	errors = mr.drawingDirFiles_rename_with_csv(dirname, csv_path)
	#TODO fazer print dos erros numa caixa de texto
	# ou numa tabela
	output_errors(errors)
	return 0




global last_dir
last_dir = "."

window = tk.Tk()
window.title("PJF Mass Renamer")
window.geometry('800x300')

# Directory
lbl_dir = tk.Label(window, text="Drawings folder")
lbl_dir.grid(column=0, row=0)
txt_dir = tk.Entry(window,width=80)
txt_dir.grid(column=1, row=0)
btn_dir = tk.Button(window, text="Browse", command = clicked_dir)
btn_dir.grid(column=2, row=0)

# CSV
lbl_csv = tk.Label(window, text="CSV File")
lbl_csv.grid(column=0, row=1)
txt_csv = tk.Entry(window,width=80)
txt_csv.grid(column=1, row=1)
btn = tk.Button(window, text="Browse", command = clicked_csv)
btn.grid(column=2, row=1)

# RUN
btn = tk.Button(window, text="RUN", command = run)
btn.grid(column=0, row=2)

# OUTPUT
txt_out = tk.Text(window, height=10)
txt_out.grid(column=1, row=3)

window.mainloop()